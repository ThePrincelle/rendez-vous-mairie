import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ViewportScroller } from '@angular/common';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';
@Component({
  selector: 'rdv-savoir-plus',
  templateUrl: './savoir-plus.component.html',
  styleUrls: ['./savoir-plus.component.scss'],
})
export class SavoirPlusComponent implements OnInit, AfterViewInit {
  breadcrumbs?: Array<BreadcrumbItem>;
  constructor(private titleService: Title, private viewportScroller: ViewportScroller) {
    this.titleService.setTitle('En savoir plus');
  }

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'En savoir plus',
        url: '/savoir-plus',
      },
    ];
  }

  ngAfterViewInit() {
    // Hack: Scrolls to top of Page after page view initialized
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }
}
