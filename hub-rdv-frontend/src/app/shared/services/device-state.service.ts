import { Injectable } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import isOnline from 'is-online';
import { Subject } from 'rxjs/internal/Subject';
import { EventManager } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root',
})
export class DeviceStateService {
  private mobileResolution: boolean = false;
  private online: boolean = true;
  private resizeSubject: Subject<boolean>;

  constructor(private eventManager: EventManager) {
    this.resizeSubject = new Subject();
    this.eventManager.addGlobalEventListener('window', 'resize', this.onResize.bind(this));

    this.isMobileResolution();
  }
  get onResize$(): Observable<boolean> {
    return this.resizeSubject.asObservable();
  }
  private onResize(event: UIEvent) {
    this.resizeSubject.next(this.isMobileResolution());
  }

  public isMobileResolution(): boolean {
    if (window.innerWidth < 768) {
      this.mobileResolution = true;
    } else {
      this.mobileResolution = false;
    }
    return this.mobileResolution;
  }

  public getScreen(): string {
    const width: number = window.innerWidth;
    let screen: string = 'LG';
    if (width < 576) {
      screen = 'XS';
    } else if (width >= 576 && width < 768) {
      screen = 'SM';
    } else if (width >= 768 && width < 992) {
      screen = 'MD';
    } else if (width >= 992 && width < 1248) {
      screen = 'LG';
    } else {
      screen = 'XL';
    }
    return screen;
  }

  public resize(): Observable<Event> {
    return fromEvent(window, 'resize');
  }

  public async isOnline() {
    return await isOnline();
  }
}
