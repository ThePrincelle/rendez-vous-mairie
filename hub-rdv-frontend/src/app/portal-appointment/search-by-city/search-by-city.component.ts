import { Component, EventEmitter, OnInit, Input, Output, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Municipality } from '@shared/classes/municipality';
import { SearchCriteria } from '@shared/classes/search-criteria';
import { AddressGouvService } from '@shared/services/address-gouv.service';
import { DeviceStateService } from '@shared/services/device-state.service';
import { PortalAppointmentService } from '@shared/services/portal-appointment.service';
import { SEOService } from '@shared/services/seoservice.service';
import { DateUtils } from '@shared/utils/date';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subscription } from 'rxjs';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-search-by-city',
  templateUrl: './search-by-city.component.html',
  styleUrls: ['./search-by-city.component.scss'],
})
/**
 * Class representing the search by city component.
 * @extends SearchByCityComponent
 */
export class SearchByCityComponent implements OnInit, OnDestroy {
  /**
   * indicator when search result is loading ( for spinner)
   * @type {boolean}
   */
  @Input() isLoadingSearchResult: boolean;
  /**
   * search criteria = form data + latitude and longitude
   * @type {SearchCriteria}
   */
  @Input() searchCriteriaIn?: SearchCriteria;
  /**
   * search criteria emitter to to the portal appointment component
   * @type {EventEmitter}
   */
  @Output() searchCriteria: EventEmitter<SearchCriteria>;
  @Output() latLong: EventEmitter<any>;
  searchRdvForm: FormGroup;
  dateMin: string;
  dateMax: string;
  subscriptions$: Subscription;
  isDesktop: boolean;
  errorMessage: any;
  /**
   * municipality
   * @type {Municipality}
   */
  municipality?: Municipality;
  hintText: string = 'Remplissage automatique des paramètres';
  checked: boolean = true;
  private resizeSubscription?: Subscription;
  breadcrumbs?: Array<BreadcrumbItem>;

  /**
   * @constructor
   * @param {DeviceStateService} deviceState - The Device State service.
   * @param {FormBuilder} formBuilder - The formBuilder service.
   * @param {Router} router - The router service.
   * @param {Meta} metaTagService - The meta tag service.
   * @param {Title} titleService - The window title service.
   * @param {PortalAppointmentService} portalAppointmentService - The PortalAppointmentService service to get Municipality by city name.
   * @param {NgxSpinnerService} spinner - The NgxSpinnerService service.
   * @param {ActivatedRoute} route - The Routing activated service.
   */
  constructor(
    private deviceState: DeviceStateService,
    private formBuilder: FormBuilder,
    private router: Router,
    private metaTagService: Meta,
    private titleService: Title,
    private portalAppointmentService: PortalAppointmentService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private addressService: AddressGouvService,
    private seoservice: SEOService
  ) {
    this.isDesktop = !this.deviceState.isMobileResolution();
    this.searchCriteria = new EventEmitter<SearchCriteria>();
    this.latLong = new EventEmitter();
    this.searchRdvForm = new FormGroup({});
    this.isLoadingSearchResult = false;
    this.dateMin = DateUtils.getDefaultStartDate();
    this.dateMax = DateUtils.getEndDateMax(this.dateMin);
    this.subscriptions$ = new Subscription();
    this.errorMessage = {};
  }

  ngOnInit(): void {
    this.resizeSubscription = this.deviceState.onResize$.subscribe((mobile: any) => {
      this.isDesktop = !mobile;
    });
    this.searchRdvForm = this.formBuilder.group({
      start_date: [DateUtils.getDefaultStartDate()],
      end_date: [DateUtils.getDefaultEndDate(null)],
      radius_km: [20, [Validators.required]],
      longitude: [null, [Validators.required]],
      latitude: [null, [Validators.required]],
      address: [''],
      reason: ['CNI', [Validators.required]],
      documents_number: [1, [Validators.required]],
    });

    let cityName = window.location.pathname;
    cityName = cityName.replace('/ville/', '');
    this.subscriptions$ = this.portalAppointmentService.getMunicipality(cityName).subscribe(
      (res) => {
        if (res) {
          this.municipality = res;

          if (this.municipality) {
            let website = this.municipality?.website;
            if (!(website?.includes('http://') || website?.includes('https://'))) {
              website = 'http://' + website;
              this.municipality.website = website;
            }
          }

          this.searchRdvForm.get('longitude')?.setValue(this.municipality?.longitude);
          this.searchRdvForm.get('latitude')?.setValue(this.municipality?.latitude);
          this.searchRdvForm.get('address')?.setValue(this.municipality?.city_name + ' ' + this.municipality?.zip_code);

          let title = 'Rendez-vous en mairie de ' + this.municipality?.city_name + " pour un passeport ou une carte d'identité";
          this.titleService.setTitle(title);

          this.metaTagService.updateTag({
            name: 'keywords',
            content:
              'rendez vous online passeport à ' +
              this.municipality?.city_name +
              ', rendez vous online passeport autour de ' +
              this.municipality?.city_name +
              ', rdv passeport à ' +
              this.municipality?.city_name +
              ', rdv passeport autour de ' +
              this.municipality?.city_name +
              ', rendez-vous mairie à ' +
              this.municipality?.city_name +
              ', rendez-vous mairie autour de ' +
              this.municipality?.city_name +
              ', rdv passeport rapide à ' +
              this.municipality?.city_name +
              ', rdv passeport rapide autour de ' +
              this.municipality?.city_name +
              ', mairie passeport rapide ' +
              this.municipality?.city_name,
          });
          this.metaTagService.addTag({
            name: 'title',
            content: title,
          });
          this.metaTagService.updateTag({
            name: 'description',
            content:
              "Trouvez rapidement un rendez-vous pour le renouvellement des passeports ou des cartes d'identité à " +
              this.municipality?.city_name +
              ' ou dans une mairie près de ' +
              this.municipality?.city_name,
          });

          const routeFragment: Observable<any> = this.route.fragment;

          routeFragment.subscribe((fragment) => {
            const searchParams = new URLSearchParams(fragment);

            if (fragment) {
              let start_date = searchParams.get('date-de-debut');
              if (!start_date) start_date = DateUtils.getDefaultStartDate();
              let end_date = searchParams.get('date-de-fin');
              if (!end_date) end_date = DateUtils.getDefaultEndDate(null);
              let radius_km = searchParams.get('distance');
              if (!radius_km) radius_km = '20';
              let documents_number = searchParams.get('nombre-de-personnes');
              if (!documents_number) documents_number = '1';
              let reason = searchParams.get('motif');
              if (!reason) reason = 'CNI';

              const criteria: SearchCriteria = {
                start_date: DateUtils.getDefaultStartDate(),
                end_date: DateUtils.getDefaultEndDate(null),
                radius_km: Number(radius_km),
                longitude: this.municipality?.longitude,
                latitude: this.municipality?.latitude,
                address: this.municipality?.city_name + ' ' + this.municipality?.zip_code,
                reason: 'CNI',
                documents_number: Number(documents_number),
              };

              this.searchCriteria.emit(criteria);
              this.setFormData(
                start_date,
                end_date,
                radius_km,
                this.municipality?.latitude,
                this.municipality?.longitude,
                documents_number,
                reason
              );
            } else {
              const criteria: SearchCriteria = {
                start_date: DateUtils.getDefaultStartDate(),
                end_date: DateUtils.getDefaultEndDate(null),
                radius_km: 20,
                longitude: this.municipality?.longitude,
                latitude: this.municipality?.latitude,
                address: this.municipality?.city_name + ' ' + this.municipality?.zip_code,
                documents_number: 1,
                reason: 'CNI',
              };

              this.searchCriteria.emit(criteria);
            }
          });
        }
        this.breadcrumbs = [
          {
            label: 'Accueil',
            url: '/',
          },
          {
            label: 'Villes',
            url: '/villes',
          },
          {
            label: this.municipality?.city_name!,
            url: '/ville/' + cityName,
          },
        ];
      },
      (error) => {
        this.addressService.getLonAndLatByAddress(window.location.pathname.replace('/ville/', ''), 'municipality').subscribe((res: any) => {
          if (res.features.length > 0) {
            let longitude = res.features[0].geometry?.coordinates[0];
            let latitude = res.features[0].geometry?.coordinates[1];

            let criteria = {
              start_date: DateUtils.getDefaultStartDate(),
              end_date: DateUtils.getDefaultEndDate(null),
              radius_km: 20,
              longitude: longitude,
              latitude: latitude,
              address: res.features[0].properties.name + ' ' + res.features[0].properties.postcode,
              reason: 'CNI',
              documents_number: 1,
            };

            this.latLong.emit(criteria);
          } else this.router.navigateByUrl('/404', { skipLocationChange: true });
        });
      }
    );

    this.seoservice.updateCanonicalUrl('https://rendezvouspasseport.ants.gouv.fr/ville/' + cityName);
  }

  /**
   * on search click, emit search criteria to the portal appointment component
   */
  onSearch(): any {
    if (this.searchRdvForm.valid) {
      const criteria: SearchCriteria = { ...this.searchRdvForm.value };
      this.searchCriteria.emit(criteria);
      this.spinner.show();
    }
  }
  /**
   * set form data from the URL using the query parameters
   */
  setFormData(
    start_date: string,
    end_date: string,
    radius_km: string,
    latitude: Number | undefined,
    longitude: Number | undefined,
    documents_number: string,
    reason: string
  ) {
    this.searchRdvForm.get('start_date')?.setValue(start_date);
    this.searchRdvForm.get('end_date')?.setValue(end_date);
    if (radius_km != '20') this.searchRdvForm.get('radius_km')?.setValue(radius_km);
    this.searchRdvForm.get('latitude')?.setValue(latitude);
    this.searchRdvForm.get('longitude')?.setValue(longitude);
    this.searchRdvForm.get('documents_number')?.setValue(documents_number);
    this.searchRdvForm.get('reason')?.setValue(reason);
  }
  /**
   * Listen to the start date select change and set the end date accordingly (+ 3 months)
   * @param {any} $event : the triggered function event
   */
  handler($event: any) {
    this.searchRdvForm.get('end_date')?.setValue(DateUtils.getDefaultEndDate(this.searchRdvForm.get('start_date')?.value));
    this.dateMax = DateUtils.getEndDateMax(this.searchRdvForm.get('start_date')?.value);
  }

  onImgError(event: any) {
    event.target.src = '/assets/icon-bank-fill.png';
    event.target.style.width = '30%';
    event.target.style.padding = '8px';
  }

  toggle(displayValue: string) {
    let rawInput = window.document.getElementById('more-settings')! as HTMLElement;
    if (rawInput.style.display == displayValue) {
      this.hintText = 'Remplissage automatique des paramètres';
      rawInput.style.display = 'none';
      this.checked = true;
    } else {
      this.hintText = 'Plus de paramètres de recherche';
      rawInput.style.display = displayValue;
      this.checked = false;
    }
  }

  /**
   * emit search criteria to the portal appointment component on destroy
   */
  ngOnDestroy() {
    const criteria: SearchCriteria = { ...this.searchRdvForm.value, resize: true };
    this.searchCriteria.emit(criteria);
    this.subscriptions$.unsubscribe();
  }
}
