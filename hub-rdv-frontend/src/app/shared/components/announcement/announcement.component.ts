import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AnnouncementService } from '@shared/services/announcement.service';

@Component({
  selector: 'rdv-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.scss'],
})
export class AnnouncementComponent implements OnInit {
  title: string = '';
  description: string = '';
  alertLevel: string = '';

  constructor(private announcementService: AnnouncementService, private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.announcementService.get().subscribe(
      (res: any) => {
        if (res) {
          this.title = res['title'];
          this.description = res['description'];
          this.alertLevel = res['alert_level'];
          this.cd.detectChanges();
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
