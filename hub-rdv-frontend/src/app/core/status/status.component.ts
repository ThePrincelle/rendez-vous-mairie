import { Component, OnInit } from '@angular/core';
import { Editor } from '@shared/classes/editor';
import { StatusService } from '@shared/services/status.service';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
})
export class StatusComponent implements OnInit {
  backendStatus: boolean = false;
  backendError: string = '';
  editors?: Editor[];
  breadcrumbs?: Array<BreadcrumbItem>;
  constructor(private statusService: StatusService) {}

  ngOnInit(): void {
    this.statusService.getStatus().subscribe(
      (res) => {
        this.backendStatus = true;
        this.editors = res.editors.sort((a: Editor, b: Editor) => (a.slug < b.slug ? -1 : 1));
      },
      (error) => {
        this.backendStatus = false;
        this.backendError = JSON.stringify(error);
      }
    );
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Statut',
        url: '/status',
      },
    ];
  }
}
