import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-deterritorialisation',
  templateUrl: './deterritorialisation.component.html',
  styleUrls: ['./deterritorialisation.component.scss'],
})
export class DeterritorialisationComponent implements OnInit {
  breadcrumbs?: Array<BreadcrumbItem>;
  constructor(private title: Title) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.title.setTitle('Informations sur la déterritorialisation');
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Informations sur la déterritorialisation',
        url: '/deterritorialisation',
      },
    ];
  }
}
