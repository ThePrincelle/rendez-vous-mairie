import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Municipality } from '@shared/classes/municipality';
import { Department } from '@shared/classes/Department';
import { Region } from '@shared/classes/Region';
import { SearchCriteria } from '@shared/classes/search-criteria';
import { DeviceStateService } from '@shared/services/device-state.service';
import { PortalAppointmentService } from '@shared/services/portal-appointment.service';
import { DateUtils } from '@shared/utils/date';
import { Meta, Title } from '@angular/platform-browser';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss'],
})
export class DepartmentsComponent implements OnInit {
  zipcodeId = '';
  departmentCodes: Map<string, string> = new Map();
  municipalities?: Array<Municipality>;
  results: Map<string, Array<Municipality>> = new Map();
  department: any;

  regions: any;

  regionsMap = new Map<string, any>();
  departemnts: any;
  breadcrumbs?: Array<BreadcrumbItem>;

  /**
   * indicator when search result is loading ( for spinner)
   * @type {boolean}
   */
  @Input() isLoadingSearchResult: boolean;
  /**
   * search criteria = form data + latitude and longitude
   * @type {SearchCriteria}
   */
  @Input() searchCriteriaIn?: SearchCriteria;
  /**
   * search criteria emitter to to the portal appointment component
   * @type {EventEmitter}
   */
  @Output() searchCriteria: EventEmitter<Array<SearchCriteria>>;
  @Output() latLong: EventEmitter<any>;
  dateMin: string;
  dateMax: string;
  desktop: boolean;
  errorMessage: any;

  constructor(
    private portalAppointmentService: PortalAppointmentService,
    private deviceState: DeviceStateService,
    private title: Title,
    private metaTagService: Meta
  ) {
    this.desktop = !this.deviceState.isMobileResolution();
    this.searchCriteria = new EventEmitter<Array<SearchCriteria>>();
    this.latLong = new EventEmitter();
    this.isLoadingSearchResult = false;
    this.dateMin = DateUtils.getDefaultStartDate();
    this.dateMax = DateUtils.getEndDateMax(this.dateMin);
    this.errorMessage = {};
  }

  ngOnInit(): void {
    this.title.setTitle('Recherche par département');
    window.scrollTo(0, 0);
    this.metaTagService.updateTag({
      name: 'description',
      content: "Trouvez rapidement un rendez-vous pour le renouvellement des passeports ou des cartes d'identité dans un département",
    });
    let departmentCodesArray: any[] = [];
    Department.getAllDepartments().forEach((dept) => {
      departmentCodesArray.push([dept['code'], dept['name']]);
    });
    this.departmentCodes = new Map<string, string>(departmentCodesArray);

    this.regions = Region.getAllRegions();
    this.regionsMap = new Map<string, string>(Object.entries(this.regions));
    let departemntDecodedName = decodeURI(window.location.pathname).replace('/departement/', '');
    this.department = Department.findDepartmentByDecodedName(departemntDecodedName);

    this.portalAppointmentService.getMunicipalities().subscribe((res) => {
      this.municipalities = res;
      if (this.municipalities) {
        let x = this.municipalities.sort((a: Municipality, b: Municipality) => Number(a.zip_code) - Number(b.zip_code));
        for (let i = 0; i < x.length; i++) {
          if (x[i].zip_code && !this.results.get((x[i].zip_code + '').slice(0, 2)))
            this.results.set((x[i].zip_code + '').trim().slice(0, 2), new Array(x[i]));
          else {
            const found = this.results.get((x[i].zip_code + '').slice(0, 2))?.some((el) => el.city_name === x[i].city_name);
            if (!found) this.results.get((x[i].zip_code + '').slice(0, 2))?.push(x[i]);
          }
        }
      }
    });
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Départements',
        url: '/departements',
      },
    ];
  }

  getKeys() {
    return Array.from(this.regionsMap.keys());
  }
}
