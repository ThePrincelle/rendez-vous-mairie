import { argsAsList, SearchCriteria } from '@shared/classes/search-criteria';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Slot } from '@shared/classes/slot';
import { DeviceStateService } from '@shared/services/device-state.service';
import { StringFormatterService } from '@shared/services/string-formatter.service';
import { WebsocketService } from '@shared/services/websocket.service';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { Observable, Subscription } from 'rxjs';
import conf from '@assets/messages/search-messages.json';
import { DateUtils } from '@shared/utils/date';
import { NgxSpinnerService } from 'ngx-spinner';
import { NativeHtmlScripts } from '@shared/services/native-html-scripts';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { SearchComponent } from '../search/search.component';
import { Department } from '@shared/classes/Department';
import { DomSanitizer } from '@angular/platform-browser';
import { PortalAppointmentService } from '@shared/services/portal-appointment.service';
//import { KeypressDistributionService } from '@shared/services/keypress-distribution.service';
import { Appointment } from '@shared/utils/appointment';

@Component({
  selector: 'rdv-portal-appointment',
  templateUrl: './portal-appointment.component.html',
  styleUrls: ['./portal-appointment.component.scss'],
})
/**
 * Class representing the portal appointment component module component, wich is which is composed of search form and the slots display.
 * @extends PredemandeComponent
 */
export class PortalAppointmentComponent implements OnInit, AfterViewInit, OnDestroy {
  private resizeSubscription?: Subscription;
  isDesktop: boolean;
  rawSlots: Array<any>;
  rawOfflineMunicipalities: Array<any>;
  /**
   * the slots data structure
   * @type {Map<string, Array<Slot>>}
   */
  slots?: Map<string, Array<Slot>>;
  OfflineMunicipalities: Map<string, Array<Slot>>;
  /**
   * subscription for websoket connection
   * @type {Subscription}
   */
  networkStatus: boolean = false;
  networkStatus$: Subscription = Subscription.EMPTY;
  subscription?: Subscription;
  scrolled: boolean;
  endOfSearch: boolean;
  results_length: number = 0;
  isLoadingSearchResult: boolean;
  searchCriteria?: SearchCriteria;
  onlyOfflineMunicipalities: boolean = true;

  /**
   * the sort by sort by feature, it is in the display slots component, declared here to set to 'by distance' if retrieval on search
   * @type {string}
   */
  sortBy: string = 'Date';
  errorMessage: any;
  /**
   * A summarizing message of the search results, and it can be an error description if there is one
   * @type {Subscription}
   */
  message: string;
  address: string = '';
  /**
   * Reference of a child component for data exchange
   * @type {SearchComponent}
   */
  @ViewChild('ref')
  childRef!: SearchComponent;
  myTimeout: any;
  /**
   * A percentage error of the editors that didn't respond
   * @type {Subscription}
   */
  non_response_percentage_from_editors?: number;
  searchByCity?: boolean;
  searchByDepartment?: boolean;

  departemntDecodedName?: string;
  localised: boolean = false;
  displayOfflineFirst?: boolean;
  smartSearch?: boolean = false;

  /**
   * A percentage error of the editors that didn't respond
   * @type {Subscription}
   */
  pathCity: string;

  /**
   * @constructor
   * @param {DeviceStateService} deviceState - The Device State service.
   * @param {WebsocketService} websocketService - The websoket service.
   * @param {ReCaptchaV3Service} recaptchaV3Service - The Google recaptcha V3 Service.
   * @param {ChangeDetectorRef} cdRef - The Change Ref Detector service.
   * @param {StringFormatterService} formatter - The String formatter service.
   * @param {NgxSpinnerService} spinner - The spinner service.
   * @param {ActivatedRoute} route - The Routing activated service.
   * @param {Router} router - The router service.
   * @param {location} location - The  window location service.
   */
  constructor(
    private deviceState: DeviceStateService,
    private websocketService: WebsocketService,
    private recaptchaV3Service: ReCaptchaV3Service,
    private formatter: StringFormatterService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private domSanitizer: DomSanitizer,
    private portalAppointmentService: PortalAppointmentService //private keyService: KeypressDistributionService
  ) {
    this.isDesktop = !this.deviceState.isMobileResolution();
    this.pathCity = '';
    if (this.router.url.includes('/ville/')) {
      this.searchByCity = true;
    } else if (decodeURI(this.router.url).includes('/departement/')) {
      this.searchByDepartment = true;
      this.departemntDecodedName = decodeURI(window.location.pathname).replace('/departement/', '');
    }

    this.rawSlots = [];
    this.rawOfflineMunicipalities = [];
    this.scrolled = false;
    this.endOfSearch = false;
    this.errorMessage = {};
    this.message = '';
    this.isLoadingSearchResult = false;
    this.OfflineMunicipalities = new Map();
  }
  /**
   * Tasks initialization
   * Check the websoket connection, and check if there are query parameters, if so, call the search function if parameters are valid
   */
  async ngOnInit() {
    this.resizeSubscription = this.deviceState.onResize$.subscribe((mobile: any) => {
      this.isDesktop = !mobile;
    });

    this.websocketService.isConnected.subscribe(async (connected: any) => {
      if (!connected.connected) {
        this.isLoadingSearchResult = false;
        this.spinner.hide();
        if (!this.errorMessage.description)
          this.errorMessage = {
            title: 'Connexion',
            description: 'Connexion au serveur impossible. Veuillez vérifier votre connexion internet ou réessayer plus tard.',
          };
      } else if (
        this.errorMessage?.description ==
        'Connexion au serveur impossible. Veuillez vérifier votre connexion internet ou réessayer plus tard.'
      )
        this.errorMessage = {};
    });

    const routeFragment: Observable<any> = this.route.fragment;
    routeFragment.subscribe((fragment) => {
      const searchParams = new URLSearchParams(fragment);

      let start_date = searchParams.get('date-de-debut');
      let end_date = searchParams.get('date-de-fin');
      let radius_km = Number(searchParams.get('distance'));
      let longitude = Number(searchParams.get('longitude'));
      let latitude = Number(searchParams.get('latitude'));
      let address = searchParams.get('address');
      let documents_number = Number(searchParams.get('nombre-de-personnes'));
      let reason = searchParams.get('motif');

      if (longitude != null && latitude != null && radius_km && end_date && start_date) {
        if (new Date(start_date) > new Date(end_date)) end_date = DateUtils.getDefaultEndDate(null);

        if (new Date(start_date) < new Date()) start_date = DateUtils.getDefaultStartDate();

        const criteria: SearchCriteria = {
          start_date: start_date,
          end_date: end_date,
          radius_km: Number(radius_km),
          longitude: longitude,
          latitude: latitude,
          address: String(address),
          documents_number: documents_number,
          reason: String(reason),
        };
        setTimeout(() => {
          if (start_date && end_date && radius_km && radius_km && latitude && latitude && address && documents_number && reason)
            this.childRef.setFormData(start_date, end_date, radius_km, latitude, longitude, address, documents_number, reason);
          this.onSearch(criteria);
        }, 1000);
      }
    });

    setTimeout(() => {
      document.getElementsByClassName('input-container')[0]?.children[0].setAttribute('id', 'Recherchez-une-ville');
      let p = document.createElement('label');
      p.textContent = 'Code postal, ville ou département';
      p.setAttribute('for', 'Recherchez-une-ville');
      p.style.display = 'none';
      document.getElementsByClassName('input-container')[0]?.prepend(p);
    }, 100);
  }

  ngAfterViewInit() {
    setTimeout(() => NativeHtmlScripts.correctRecaptchaAccessibility(), 1000);
  }

  /**
   * This trigged method when "trier par" drop down list value change
   * @param {any} sortBy : 'Date' or 'Distance'
   */
  onSortByChange(sortBy: any) {
    this.sortBy = sortBy;
    this.organizeSlots();
  }

  /**
   * search method with
   * @param {SearchCriteria} searchCriteria : search criteria which is json object with the folowing keys : start_date, end_date, radius_km, latitude, longitude, address, documents_number, reason
   * this method send the search criteria to the api (backend) using the websocket, and gathers the received data (slots) in streaming mode
   */
  onSearch(searchCriteria: SearchCriteria) {
    if (searchCriteria.latitude) {
      if (this.searchByDepartment) searchCriteria.radius_km = 100;

      this.searchCriteria = searchCriteria;
      if (!Object(this.searchCriteria)['resize']) {
        let path;

        if (this.searchByCity) {
          path =
            window.location.pathname +
            '#date-de-debut=' +
            this.searchCriteria['start_date'] +
            '&date-de-fin=' +
            this.searchCriteria['end_date'] +
            '&distance=' +
            this.searchCriteria['radius_km'] +
            '&nombre-de-personnes=' +
            this.searchCriteria['documents_number'] +
            '&motif=' +
            this.searchCriteria['reason'];
        } else if (this.searchByDepartment) {
          path =
            window.location.pathname +
            '#date-de-debut=' +
            this.searchCriteria['start_date'] +
            '&date-de-fin=' +
            this.searchCriteria['end_date'] +
            '&nombre-de-personnes=' +
            this.searchCriteria['documents_number'] +
            '&motif=' +
            this.searchCriteria['reason'];
        } else {
          path =
            window.location.pathname +
            '#date-de-debut=' +
            this.searchCriteria['start_date'] +
            '&date-de-fin=' +
            this.searchCriteria['end_date'] +
            '&distance=' +
            this.searchCriteria['radius_km'] +
            '&latitude=' +
            this.searchCriteria['latitude'] +
            '&longitude=' +
            this.searchCriteria['longitude'] +
            '&address=' +
            this.searchCriteria['address'] +
            '&nombre-de-personnes=' +
            this.searchCriteria['documents_number'] +
            '&motif=' +
            this.searchCriteria['reason'];
        }

        this.location.go(path);

        if (!this.websocketService.isWsOpen()) {
          this.errorMessage = {
            title: 'Connexion',
            description: 'Connexion au serveur impossible. Veuillez réessayer plus tard.',
          };
        }

        this.errorMessage = {};
        this.slots = new Map();
        this.OfflineMunicipalities = new Map();
        this.rawSlots = [];
        this.rawOfflineMunicipalities = [];
        this.results_length = 0;
        this.message = '';
        this.onlyOfflineMunicipalities = true;

        this.portalAppointmentService.getMunicipalitiesOffline(this.searchCriteria).subscribe((res: Array<Slot>) => {
          this.OfflineMunicipalities = Appointment.groupMunicipaltiesByDistance(res);

          this.OfflineMunicipalities.forEach((municipality: Slot[]) => {
            municipality = municipality.sort(function (a: any, b: any) {
              if (a.distance < b.distance) return -1;
              else return 1;
            });
            this.rawOfflineMunicipalities.push.apply(this.rawOfflineMunicipalities, municipality);
          });
        });

        this.errorMessage = {};
        this.slots = new Map();
        this.rawSlots = [];
        this.results_length = 0;
        this.message = '';
        this.rawOfflineMunicipalities = [];
        this.onlyOfflineMunicipalities = true;

        this.isLoadingSearchResult = true;
        this.non_response_percentage_from_editors = 0;

        if (!this.subscription || this.subscription.closed) {
          this.subscription = this.websocketService.messageSubject?.subscribe({
            next: (response) => {
              let new_slots = JSON.parse(response.data);

              if (response.data.includes('Erreur')) {
                this.errorMessage = {
                  title: '',
                  description: 'Captcha invalide',
                };

                this.isLoadingSearchResult = false;
                this.websocketService.ws?.close();
                this.slots = new Map();
                this.subscription = undefined;
                this.endOfSearch = false;
                this.spinner.hide();
              } else if (!response.data.includes('end_of_search')) {
                this.spinner.show();
                this.endOfSearch = false;
                this.isLoadingSearchResult = true;

                this.rawSlots = this.rawSlots.concat(new_slots);

                this.organizeSlots();

                if (this.results_length != undefined && this.searchCriteria) {
                  const args: Array<string> = argsAsList(this.searchCriteria);

                  //department or city name with zipcode
                  let localion_name = args[2];

                  if (!this.searchByDepartment) args[2] = localion_name.slice(0, -5);
                  else
                    args[2] =
                      this.searchCriteria.department_code +
                      ' - ' +
                      localion_name.slice(0, -5).charAt(0).toUpperCase() +
                      localion_name.slice(0, -5).slice(1).toLowerCase();

                  args.push(this.results_length?.toString());

                  this.message = this.formatter.format(this.getMessage(), ...args);
                }
              } else {
                if (this.searchCriteria) {
                  if (this.smartSearch) {
                    this.searchCriteria.end_date = new_slots.settings.end_date;
                    this.searchCriteria.radius_km = new_slots.settings.radius;
                  }

                  const args: Array<string> = argsAsList(this.searchCriteria);
                  //department or city name with zipcode
                  let localion_name = args[2];
                  if (!this.searchByDepartment) args[2] = localion_name.slice(0, -5);
                  else
                    args[2] =
                      this.searchCriteria.department_code +
                      ' - ' +
                      localion_name.slice(0, -5).charAt(0).toUpperCase() +
                      localion_name.slice(0, -5).slice(1).toLowerCase();

                  args.push(this.results_length?.toString());
                  this.message = this.formatter.format(this.getMessage(), ...args);
                }

                if (new_slots.editor_errors_number > 0) {
                  let percentage = (new_slots.editor_errors_number / new_slots.editors_number) * 100;
                  this.non_response_percentage_from_editors = Math.floor(percentage);
                }

                if (this.message === '' && this.searchCriteria) {
                  const args: Array<string> = argsAsList(this.searchCriteria);
                  if (!this.searchByDepartment) this.message = this.formatter.format(conf.messages.noSlots, ...args);
                  else this.message = this.formatter.format(conf.messages.noSlotsdepartment, ...args);
                }
                this.isLoadingSearchResult = false;
                this.endOfSearch = true;
                this.spinner.hide();
                setTimeout(() => {
                  this.scroll();
                }, 100);
              }
            },
          });
        } else if (!this.endOfSearch) {
          this.isLoadingSearchResult = false;
          this.websocketService.ws?.close();
          this.subscription.unsubscribe();
          this.slots = new Map();
          this.subscription = undefined;
          this.endOfSearch = false;
          this.spinner.hide();
        }
        if (this.subscription)
          this.recaptchaV3Service.execute('myAction').subscribe({
            next: (token: any) => {
              clearTimeout(this.myTimeout);
              this.myTimeout = setTimeout(() => {
                this.checkOverload();
              }, 20000);
              this.spinner.show();
              this.endOfSearch = false;
              try {
                if (this.searchByDepartment && this.departemntDecodedName) {
                  let department: any = Department.findDepartmentByDecodedName(this.departemntDecodedName);
                  searchCriteria['department_code'] = department['code'];
                  searchCriteria['address'] =
                    department['name'] + ' ' + department['code'] + (department['code'].length == 2 ? '000' : '00');
                }
                searchCriteria['reCAPTCHA_token'] = token;

                let rawInput = window.document.getElementById('more-settings')! as HTMLElement;
                if (rawInput)
                  if (rawInput.style.display == 'none' || !rawInput.style.display) {
                    this.smartSearch = true;
                    searchCriteria['smart_search'] = true;
                  } else {
                    this.smartSearch = false;
                    searchCriteria['smart_search'] = false;
                  }

                this.websocketService.messageSubject?.next(searchCriteria);
                /*
                if (this.searchCriteria) {
                  this.searchCriteria.start_date = DateUtils.getDateFormat(this.searchCriteria.start_date);
                  this.searchCriteria.end_date = DateUtils.getDateFormat(this.searchCriteria.end_date);
                }
                */
              } catch (error) {
                if (this.websocketService.isWsOpen()) this.subscription?.unsubscribe();
                this.subscription = undefined;
                this.endOfSearch = true;
                this.spinner.hide();
                // TODO show an error message : server temporarely unavailable
              }
              this.scroll();
            },
            error: (e: any) => {
              console.error(`Recaptcha v3 error:`, e);
              this.isLoadingSearchResult = false;
            },
          });
      }
    }
  }

  /**
   * Check if there is an overload on the system, if so, display a message
   */
  checkOverload() {
    if (this.slots?.size == 0 && !this.endOfSearch) {
      this.errorMessage = {
        title: 'Connexion',
        description: 'La connexion au serveur est impossible actuellement. Nous vous invitons à renouveler votre recherche ultérieurement',
      };
      this.isLoadingSearchResult = false;
      this.endOfSearch = true;
      this.spinner.hide();

      setTimeout(() => {
        let scrolltoelement = window.document.getElementById('error')! as HTMLInputElement;
        if (scrolltoelement) {
          scrolltoelement.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
            inline: 'nearest',
          });
        }
      }, 100);
    }
  }

  /**
   * Slots creation in json format according to the sortBy parameter
   */
  initiateSlotsMap(): [Map<string, Array<Slot>>, Array<Slot>] {
    let slotspertabs = new Map();
    let array = new Array();
    this.results_length = 0;
    if (this.sortBy === 'Date') {
      this.rawSlots.forEach((mairie: any) => {
        mairie.available_slots.forEach((s: any) => {
          let d;

          if (s['datetime'].slice(-1) === 'Z') d = new Date(s['datetime'].replace('Z', ''));
          else d = new Date(s['datetime']);

          if (d >= new Date()) {
            this.results_length = this.results_length + 1;
            const slot: Slot = new Slot();
            slot.municipality = mairie['name'];
            slot.distance = mairie['distance_km'].toFixed(1);
            slot.url = s.callback_url;

            slot.hour = d.toString();
            // slot.logo = this.domSanitizer.bypassSecurityTrustUrl(mairie['city_logo'] || '/assets/icon-bank-fill.png');
            slot.logo = this.domSanitizer.bypassSecurityTrustUrl(mairie['city_logo']);

            if (mairie['name'] == 'Mairie de Marquette-lez-Lille') slot.logo = '';

            slot.publicEntryAddress = mairie['public_entry_address'];
            slot.zipCode = mairie['zip_code'];
            slot.cityName = mairie['city_name'];
            slotspertabs.set(d.toISOString().slice(0, 10), new Array());
            array.push(slot);
          }
        });
      });
    } else {
      let ranges = 0;
      if (this.searchByDepartment && this.searchCriteria) this.searchCriteria.radius_km = 100;
      if (this.searchCriteria?.radius_km) {
        ranges = this.searchCriteria.radius_km / 5;
        if (this.searchByDepartment) ranges = 100 / 5;
        for (let i = 0; i < ranges; i++) slotspertabs.set(((i + 1) * 5).toString(), new Array());
      }
      this.rawSlots.forEach((mairie: any) => {
        mairie.available_slots.forEach((s: any) => {
          let d;

          if (s['datetime'].slice(-1) === 'Z') d = new Date(s['datetime'].replace('Z', ''));
          else d = new Date(s['datetime']);

          if (d >= new Date()) {
            this.results_length = this.results_length + 1;
            const slot: Slot = new Slot();
            slot.municipality = mairie['name'];
            slot.distance = mairie['distance_km'].toFixed(1);
            // slot.url = this.domSanitizer.bypassSecurityTrustUrl(s.callback_url);
            slot.url = s.callback_url;

            slot.hour = d.toString();
            slot.logo = this.domSanitizer.bypassSecurityTrustUrl(mairie['city_logo']);

            if (mairie['name'] == 'Mairie de Marquette-lez-Lille') slot.logo = '';

            slot.publicEntryAddress = mairie['public_entry_address'];
            slot.zipCode = mairie['zip_code'];
            slot.cityName = mairie['city_name'];
            array.push(slot);
          }
        });
      });
    }

    return [slotspertabs, array];
  }

  /**
   * Grouping of slots by 'Date' or distance range
   */
  distributeSlots(slotsperdate: Map<string, Array<Slot>>, slotsArray: Array<Slot>) {
    if (this.sortBy === 'Date') {
      slotsArray.forEach((slot: Slot) => {
        if (slot.hour) {
          let date = new Date(slot.hour);
          slotsperdate.get(date.toISOString().slice(0, 10))?.push(slot);
        }
      });
    } else {
      if (this.searchByDepartment && this.searchCriteria) this.searchCriteria.radius_km = 100;
      if (this.searchCriteria?.radius_km) {
        let ranges = this.searchCriteria.radius_km / 5;
        slotsArray.forEach((slot: Slot) => {
          for (let i = 0; i < ranges; i++) {
            if (slot.distance < (i + 1) * 5 && slot.distance >= (i + 1) * 5 - 5) slotsperdate.get(((i + 1) * 5).toString())?.push(slot);
          }
        });
      }
    }
  }

  /**
   * Handle the sent search cretaria from search by city when this city doesn't existe in our search engine
   * @param {any} $event : search criteria which is json object with the folowing keys : start_date, end_date, radius_km, latitude, longitude, address, documents_number, reason
   */
  setLatLong($event: any) {
    this.searchByCity = false;
    this.searchCriteria = $event;
    if (this.searchCriteria)
      setTimeout(() => {
        if (
          this.searchCriteria?.start_date &&
          this.searchCriteria.end_date &&
          this.searchCriteria.radius_km &&
          this.searchCriteria.latitude &&
          this.searchCriteria.longitude &&
          this.searchCriteria.address &&
          this.searchCriteria.documents_number &&
          this.searchCriteria.reason
        )
          this.childRef.setFormData(
            this.searchCriteria.start_date,
            this.searchCriteria?.end_date,
            this.searchCriteria?.radius_km,
            this.searchCriteria?.latitude,
            this.searchCriteria?.longitude,
            this.searchCriteria?.address,
            this.searchCriteria?.documents_number,
            this.searchCriteria?.reason
          );
      }, 200);
  }

  /**
   * sort all slots by date
   * @param {Map<any, Array<Slot>>} slotsperdate : all slots
   */
  //  sortSlots(slotsperdate: Map<string, Array<Slot>>) {
  //  sortSlots(slotsperdate: Map<string, Array<Slot>>) {
  sortSlots(slotsperdate: Map<any, Array<Slot>>) {
    if (this.sortBy == 'Date')
      slotsperdate.forEach((slotsArray: Array<Slot>) => {
        slotsArray = slotsArray.sort(function (a: any, b: any) {
          if (<any>new Date(a._hour ? a._hour : '') > <any>new Date(b._hour ? b._hour : '')) return 1;
          if (<any>new Date(a._hour ? a._hour : '') < <any>new Date(b._hour ? b._hour : '')) return -1;
          if (a._distance < b._distance) return -1;
          else return 1;
        });
      });
    else
      slotsperdate.forEach((slotsArray: Array<Slot>) => {
        slotsArray = slotsArray.sort(function (a: any, b: any) {
          if (a._distance < b._distance) return -1;
          else return 1;
        });
      });
  }

  /**
   * organize slots by grouping and sorting
   */
  organizeSlots() {
    //if(this.rawSlots.length>0) this.onlyOfflineMunicipalities = false;
    this.slots = new Map();
    let slotspertabs: Map<string, Array<Slot>>;
    let slotsArray: Array<Slot>;
    [slotspertabs, slotsArray] = this.initiateSlotsMap();
    this.distributeSlots(slotspertabs, slotsArray);

    this.sortSlots(slotspertabs);

    if (this.sortBy == 'Distance')
      for (let [key, value] of slotspertabs.entries()) {
        if (value.length) this.slots.set(key, value);
      }
    else this.slots = slotspertabs;

    if (this.slots.size) this.onlyOfflineMunicipalities = false;
  }

  sortDates(list: Array<string>) {
    return list.sort((a, b) => {
      return <any>new Date(a) - <any>new Date(b);
    });
  }

  /**
   * Scroll down to the displayed results
   */
  scroll() {
    if (this.slots?.size !== 0 || this.endOfSearch) {
      let scrolltoelement = window.document.getElementById('target')! as HTMLInputElement;
      if (scrolltoelement) {
        scrolltoelement.scrollIntoView({
          behavior: 'smooth',
          block: 'start',
          inline: 'nearest',
        });
        this.scrolled = true;
      }
    } else
      setTimeout(() => {
        this.scroll();
      }, 500);
  }
  /**
   * Get the results descriptive message
   */
  getMessage(): string {
    let emptyOffline: boolean = true;
    if (this.rawOfflineMunicipalities.length === 0) {
      emptyOffline = true;
    } else {
      emptyOffline = false;
    }
    if (!this.searchByDepartment) {
      if (this.results_length === 0) {
        return conf.messages.noSlots;
      } else if (emptyOffline && this.results_length === 1) {
        return conf.messages.oneSlot;
      } else if (emptyOffline && this.results_length > 1) {
        return conf.messages.manySlots;
      } else if (!emptyOffline && this.results_length > 1) {
        return conf.messages.manySlotsWithOffline;
      } else if (!emptyOffline && this.results_length === 1) {
        return conf.messages.oneSlotOffline;
      }
    } else {
      if (this.results_length === 0) {
        return conf.messages.noSlotsdepartment;
      } else if (this.results_length === 1) {
        return conf.messages.oneSlotdepartment;
      } else if (this.results_length && this.results_length > 1) {
        return conf.messages.manySlotsdepartment;
      }
    }

    return '';
  }

  getFirstSlotWeight(firstSlot: any) {
    if (!firstSlot) return 1;
    else {
      var diff = (new Date().getTime() - <any>new Date(firstSlot._hour)) / 1000;
      diff = Math.abs(Math.round(diff / (60 * 60 * 24 * 7)));
      if (
        diff > 8 ||
        firstSlot._distance > 40 ||
        (firstSlot._distance >= 30 && firstSlot._distance <= 40 && diff >= 4 && diff <= 8) ||
        (firstSlot._distance >= 20 && firstSlot._distance <= 30 && diff >= 6 && diff <= 8)
      )
        return 0;
    }

    return 1;
  }

  onLocalize(localised: any) {
    this.localised = localised;
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe;
    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }
  }
  onResize(event: any) {
    let new_desktop = !this.deviceState.isMobileResolution();
    if (new_desktop != this.isDesktop) {
      this.isDesktop = new_desktop;
    }
  }
}
