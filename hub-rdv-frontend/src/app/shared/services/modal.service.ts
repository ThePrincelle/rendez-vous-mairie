import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  showLoading: EventEmitter<any>;
  showGeoUndefined: EventEmitter<any>;
  showModalRedirection: EventEmitter<any>;
  constructor() {
    this.showLoading = new EventEmitter<any>();
    this.showGeoUndefined = new EventEmitter<any>();
    this.showModalRedirection = new EventEmitter<any>();
  }

  public showModal(cpName: string) {
    this.showLoading.emit({ loading: true, cpName: cpName });
  }

  public showModalGeoUndefined() {
    this.showGeoUndefined.emit({ showGeoUndefined: true });
  }

  public showModalRedirectionPred(url: string | undefined) {
    this.showModalRedirection.emit({ showModalRedirection: true, url: url });
  }
}
