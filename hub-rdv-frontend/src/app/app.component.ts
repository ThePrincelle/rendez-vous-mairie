import { Component, OnInit, Inject } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';
import { isDevMode } from '@angular/core';
import { environment } from '../environments/environment';
import { isPlatformBrowser } from '@angular/common';
import { PLATFORM_ID } from '@angular/core';
//import { KeypressDistributionService } from '@shared/services/keypress-distribution.service';
@Component({
  selector: 'rdv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  private script1?: HTMLScriptElement;
  private script2?: HTMLScriptElement;
  /*
  @HostListener('document:keyup', ['$event'])
  public onKeyUp(eventData: KeyboardEvent) {
    this.keyService.distributeKeyPress(eventData);
  }
  */
  constructor(
    //private keyService: KeypressDistributionService,
    router: Router,
    private metaTagService: Meta,
    @Inject(PLATFORM_ID) private platformId: any
  ) {
    router.events.subscribe((val) => {
      // see also
      if (val instanceof NavigationEnd) {
        if (isPlatformBrowser(this.platformId)) {
          this.launchJs();
        }
      }
    });
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      if (window.location.hostname === 'dev.rendez-vous.france-identite.fr')
        window.location.replace('https://ppd.rendezvouspasseport.ants.gouv.fr');

      if (window.location.hostname === 'rendez-vous.france-identite.fr')
        window.location.replace('https://rendezvouspasseport.ants.gouv.fr');

      if (window.location.pathname == '/') {
        this.metaTagService.addTags([
          {
            name: 'description',
            content: "Trouvez rapidement un rendez-vous pour un passeport ou une carte d'identité dans une mairie près de chez vous.",
          },
        ]);
      } else if (window.location.pathname == '/predemande/NumeroPredemande') {
        this.metaTagService.addTags([
          {
            name: 'description',
            content: 'Modifier ou annuler vos rendez-vous en mairie(s) à partir de numéro(s) de pré-demande.',
          },
        ]);
      }
    }

    if (isDevMode()) {
      this.metaTagService.addTags([
        {
          name: 'robots',
          content: 'noindex',
        },
      ]);
    } else {
      this.metaTagService.addTags([
        {
          name: 'robots',
          content: 'index, follow',
        },
      ]);
    }

    this.metaTagService.addTags([
      {
        name: 'keywords',
        content:
          "rendez vous on line passeport, rdv passeport, rdv passeport rapide, mairie passeport rapide, ants passeport, ants gouv, ants mon compte, passeport, carte d'identité, rendez-vous mairie, agence nationale des titres sécurisé",
      },
      {
        name: 'google-site-verification',
        content: environment.googleSiteVerification,
      },
    ]);
  }

  private launchJs() {
    if (this.script1) this.script1.remove();

    if (this.script2) this.script2.remove();

    let s = document.createElement('script');
    s.type = 'module';
    s.src = '/@gouvfr/dsfr/dsfr.module.min.js';
    let head = document.getElementsByTagName('head')[0];
    head.appendChild(s);
    this.script1 = s;

    s = document.createElement('script');
    s.type = 'text/javascript';
    s.noModule = true;
    s.src = '/@gouvfr/dsfr/dsfr.nomodule.min.js';
    head = document.getElementsByTagName('head')[0];
    head.appendChild(s);
    this.script2 = s;
  }
}
