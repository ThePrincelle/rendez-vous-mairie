import { Component, EventEmitter, OnInit, Input, Output, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { SearchCriteria } from '@shared/classes/search-criteria';
import { AddressGouvService } from '@shared/services/address-gouv.service';
import { DeviceStateService } from '@shared/services/device-state.service';
import { GeolocalisationService } from '@shared/services/geolocalisation.service';
import { ModalService } from '@shared/services/modal.service';
import { SEOService } from '@shared/services/seoservice.service';
import { DateUtils } from '@shared/utils/date';
import { DateValidator } from '@shared/validators/date.validator';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';

@Component({
  selector: 'rdv-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {
  @Input() isLoadingSearchResult: boolean;
  @Input() searchCriteriaIn?: SearchCriteria;
  @Output() searchCriteria: EventEmitter<SearchCriteria>;
  @Output() localised: EventEmitter<boolean>;
  @ViewChild('autocomplete1') auto: any;
  private resizeSubscription: Subscription;

  searchRdvForm: FormGroup;
  address: string;
  public data: Object[];
  isLoadingResult: boolean;
  keyword: string;
  dateMin: string;
  dateMax: string;
  userPosition?: string;
  subscriptions$: Subscription;
  subscriptionGetCurrentPosition$?: Subscription;
  subscriptionGetAddress$?: Subscription;
  subscriptionGetLonAndLat$?: Subscription;
  subscriptionGetLonAndLat1$?: Subscription;
  isDesktop: boolean;

  hintText: string = 'Remplissage automatique des paramètres';

  checked: boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private addressService: AddressGouvService,
    private geolocalisation: GeolocalisationService,
    private modalService: ModalService,
    private spinner: NgxSpinnerService,
    private deviceState: DeviceStateService,
    private seoservice: SEOService,
    private title: Title
  ) {
    this.isDesktop = !this.deviceState.isMobileResolution();
    this.resizeSubscription = this.deviceState.onResize$.subscribe((mobile: any) => {
      const criteria: SearchCriteria = { ...this.searchRdvForm.value, resize: true };
      this.searchCriteria.emit(criteria);
      this.isDesktop = !mobile;
    });
    this.localised = new EventEmitter<boolean>();
    this.searchCriteria = new EventEmitter<SearchCriteria>();
    this.searchRdvForm = new FormGroup({});
    this.data = [{ tagName: '' }];
    this.address = '';
    this.keyword = 'tagName';
    this.isLoadingSearchResult = false;
    this.isLoadingResult = false;
    this.dateMin = DateUtils.getDefaultStartDate();
    this.dateMax = DateUtils.getEndDateMax(this.dateMin);
    this.subscriptions$ = new Subscription();
  }

  ngOnInit(): void {
    this.title.setTitle('Recherche de rendez-vous en mairie');
    this.searchRdvForm = this.formBuilder.group(
      {
        start_date: [DateUtils.getDefaultStartDate()],
        end_date: [DateUtils.getDefaultEndDate(null)],
        radius_km: [20, [Validators.required]],
        longitude: [null, [Validators.required]],
        latitude: [null, [Validators.required]],
        address: [''],
        reason: ['CNI', [Validators.required]],
        documents_number: [1, [Validators.required]],
      },
      { validator: [DateValidator.dateRangeValidator, DateValidator.dateFormatValidator, DateValidator.startDateRangeValidator] }
    );
    if (this.searchCriteriaIn) this.updateForm();

    document.getElementsByClassName('input-container')[0]?.children[0].setAttribute('aria-expanded', 'true');
    this.fixAutocompleteAccessibilty();

    this.seoservice.updateCanonicalUrl('https://rendezvouspasseport.ants.gouv.fr');
  }

  toggle_expend_char($event: any) {
    let elm = document.getElementById('expend-char');
    if (elm) elm.classList.toggle('toggle');
  }

  fixAutocompleteAccessibilty() {
    window.addEventListener('load', function () {
      document.getElementsByClassName('material-icons')[0]?.setAttribute('aria-label', 'effacer la saisie');
      document.getElementsByClassName('autocomplete-container')[0]?.removeAttribute('aria-expanded');
    });
  }

  updateForm() {
    if (this.searchCriteriaIn) {
      if (this.searchCriteriaIn.address) this.address = this.searchCriteriaIn.address;
      this.searchRdvForm.patchValue({
        start_date: DateUtils.getDate(this.searchCriteriaIn.start_date),
        end_date: DateUtils.getDate(this.searchCriteriaIn.end_date),
        radius_km: this.searchCriteriaIn.radius_km,
        address: this.searchCriteriaIn.address,
        reason: this.searchCriteriaIn.reason,
        documents_number: this.searchCriteriaIn.documents_number,
        latitude: this.searchCriteriaIn.latitude,
        longitude: this.searchCriteriaIn.longitude,
      });
    }
  }

  onSearch(): any {
    if (this.searchRdvForm.valid) {
      const criteria: SearchCriteria = { ...this.searchRdvForm.value };
      this.searchCriteria.emit(criteria);
    }
  }

  getUserLocation() {
    let state: string;

    navigator.permissions.query({ name: 'geolocation' }).then((result) => {
      let modal = document.getElementById('fr-modal-0');
      if (result.state === 'prompt') {
        state = 'prompot';
        modal?.classList.add('fr-modal--opened');
      } else if (result.state === 'denied') {
        modal?.classList.remove('fr-modal--opened');
      }

      this.subscriptionGetCurrentPosition$ = this.geolocalisation.getCurrentPosition().subscribe(
        (position: any) => {
          this.spinner.show();
          this.isLoadingSearchResult = true;
          modal?.classList.remove('fr-modal--opened');
          this.subscriptionGetAddress$ = this.addressService.getAddressByLonAndLat(position.longitude, position.latitude).subscribe(
            (res) => {
              if (res.features?.length > 0) {
                // Replace some faulse zip codes
                if (res.features[0].properties.city == 'Colombier-Saugnieu') {
                  res.features[0].properties.postcode = '69124';
                } else if (res.features[0].properties.city == 'Annecy') {
                  res.features[0].properties.postcode = '74000';
                } else if (res.features[0].properties.city == 'La Teste-de-Buch') {
                  res.features[0].properties.postcode = '33260';
                } else if (res.features[0].properties.city == 'Évron') {
                  res.features[0].properties.postcode = '53600';
                } else if (res.features[0].properties.city == 'Saint-Raphaël' && res.features[0].properties.postcode == '83530') {
                  res.features[0].properties.postcode = '83700';
                }
                this.address = res.features[0].properties.city + ' ' + res.features[0].properties.postcode;
                this.data = [];
                this.searchRdvForm.get('longitude')?.setValue(position.longitude);
                this.searchRdvForm.get('latitude')?.setValue(position.latitude);
                this.spinner.hide();
                this.isLoadingSearchResult = false;
                this.localised.emit(true);
              } else {
                this.modalService.showModalGeoUndefined();
                this.isLoadingResult = false;
                this.isLoadingSearchResult = false;
                this.spinner.hide();
              }
            },
            (error: any) => {
              console.error(error);
              this.isLoadingResult = false;
              this.isLoadingSearchResult = false;
              this.spinner.hide();
            }
          );
        },
        (error: GeolocationPositionError) => {
          modal?.classList.remove('fr-modal--opened');
          this.isLoadingResult = false;
          this.isLoadingSearchResult = false;
          this.modalService.showModal('desktop');
          this.spinner.hide();
        }
      );
      this.subscriptions$.add(this.subscriptionGetCurrentPosition$);
      this.subscriptions$.add(this.subscriptionGetAddress$);
    });
  }

  onChangeSearch(search: string) {
    let array: any = [];
    this.data = [];
    this.address = '';
    this.searchRdvForm.get('longitude')?.setValue(null);
    this.searchRdvForm.get('latitude')?.setValue(null);
    if (search.length !== 0) {
      this.isLoadingResult = true;
      this.subscriptionGetLonAndLat$ = this.addressService.getLonAndLatByAddress(search, 'municipality').subscribe(
        (res: any) => {
          if (res.features.length > 0) {
            res.features.forEach((features: any) => {
              // Replace some faulse zip codes
              if (features.properties.city == 'Colombier-Saugnieu') {
                features.properties.postcode = '69124';
              } else if (features.properties.city == 'Annecy') {
                features.properties.postcode = '74000';
              } else if (features.properties.city == 'La Teste-de-Buch') {
                features.properties.postcode = '33260';
              } else if (features.properties.city == 'Évron') {
                features.properties.postcode = '53600';
              } else if (features.properties.city == 'Saint-Raphaël' && features.properties.postcode == '83530') {
                features.properties.postcode = '83700';
              }
              array.push({
                tagName: features.properties.city + ' ' + features.properties.postcode,
                coordinates: features.geometry?.coordinates,
              });
            });
          }
          this.isLoadingResult = false;
          if (array.length > 0) {
            this.data = [];
            var iterator = array.values();
            for (let elements of iterator) this.data.push(elements);
          }
        },
        (error) => {
          this.isLoadingResult = false;
        }
      );
    } else {
      this.data = [];
    }
    this.subscriptions$.add(this.subscriptionGetLonAndLat$);
  }

  selectEvent(item: any) {
    if (typeof item == 'object')
      if (item.coordinates) {
        this.searchRdvForm.get('longitude')?.setValue(item.coordinates[0]);
        this.searchRdvForm.get('latitude')?.setValue(item.coordinates[1]);
        this.localised.emit(false);
      } else {
        this.subscriptionGetLonAndLat1$ = this.addressService.getLonAndLatByAddress(item.tagName).subscribe((res: any) => {
          let coordinates = res.features[0].geometry.coordinates;
          this.searchRdvForm.get('longitude')?.setValue(coordinates[0]);
          this.searchRdvForm.get('latitude')?.setValue(coordinates[1]);
          this.localised.emit(false);
        });
      }
    this.searchRdvForm.get('address')?.setValue(item.tagName == undefined ? item : item.tagName);
    this.subscriptions$.add(this.subscriptionGetLonAndLat1$);
  }

  onCleared(e: any) {
    this.searchRdvForm.get('longitude')?.setValue(null);
    this.searchRdvForm.get('latitude')?.setValue(null);
    this.data = [{ tagName: '' }];
    this.address = '';
    this.auto.focus();
  }

  setRaduis(radius: number) {
    this.searchRdvForm.get('radius_km')?.setValue(radius);
  }

  setFormData(
    start_date: string,
    end_date: string,
    radius_km: number,
    latitude: number,
    longitude: number,
    address: string,
    documents_number: number,
    reason: string
  ) {
    this.searchRdvForm.get('start_date')?.setValue(start_date);
    this.searchRdvForm.get('end_date')?.setValue(end_date);
    this.searchRdvForm.get('radius_km')?.setValue(radius_km);
    if (address) this.address = address;
    this.searchRdvForm.get('latitude')?.setValue(latitude);
    this.searchRdvForm.get('longitude')?.setValue(longitude);
    this.searchRdvForm.get('documents_number')?.setValue(documents_number);
    this.searchRdvForm.get('reason')?.setValue(reason);
  }

  customFilter = function (data: any[], query: string): any[] {
    return data.filter((x) => x.tagName);
  };

  handler($event: any) {
    //this.dateMin = this.searchRdvForm.get('start_date')?.value;
    this.searchRdvForm.get('end_date')?.setValue(DateUtils.getDefaultEndDate(this.searchRdvForm.get('start_date')?.value));
    this.dateMax = DateUtils.getEndDateMax(this.searchRdvForm.get('start_date')?.value);
  }

  onFocus() {
    let scrolltoelement = window.document.getElementById('autocomplete')! as HTMLInputElement;
    if (scrolltoelement) {
      scrolltoelement.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest',
      });
    }
  }

  toggle(displayValue: string) {
    let rawInput = window.document.getElementById('more-settings')! as HTMLElement;
    if (rawInput.style.display == displayValue) {
      this.hintText = 'Remplissage automatique des paramètres';
      rawInput.style.display = 'none';
      this.checked = true;
    } else {
      this.hintText = 'Plus de paramètres de recherche';
      rawInput.style.display = displayValue;
      this.checked = false;
    }
  }
  ngOnDestroy() {
    const criteria: SearchCriteria = { ...this.searchRdvForm.value, resize: true };
    this.searchCriteria.emit(criteria);
    this.subscriptions$.unsubscribe();
    if (this.resizeSubscription) {
      this.resizeSubscription.unsubscribe();
    }
  }
}
