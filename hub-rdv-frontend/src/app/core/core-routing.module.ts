import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessibilityComponent } from './accessibility/accessibility.component';
import { CguMentionLegalComponent } from './cgu-mention-legal/cgu-mention-legal.component';
import { DeterritorialisationComponent } from './deterritorialisation/deterritorialisation.component';
import { DonneesPersonnellesComponent } from './donnees-personnelles/donnees-personnelles.component';
import { SavoirPlusComponent } from './savoir-plus/savoir-plus.component';
import { StatusComponent } from './status/status.component';

export const routes: Routes = [
  { path: 'CGU-mentions-legales', component: CguMentionLegalComponent },
  { path: 'status', component: StatusComponent },
  { path: 'savoir-plus', component: SavoirPlusComponent },
  { path: 'accessibilite', component: AccessibilityComponent },
  { path: 'deterritorialisation', component: DeterritorialisationComponent },
  { path: 'donnees-personnelles', component: DonneesPersonnellesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoreRoutingModule {}
