from datetime import date, timedelta

import pytest

from src.hubrdvmairie.services.search_time_slots import search_slots
from tests.mocks import editor as editor_mock
from tests.mocks import meeting_point as meeting_points_mock


@pytest.mark.asyncio
async def test_search_slots_ok(mocker):
    mocker.patch(
        "src.hubrdvmairie.services.search_time_slots.get_all_meeting_points",
        return_value=meeting_points_mock.list_meeting_points,
    )
    mocker.patch(
        "src.hubrdvmairie.services.search_time_slots.search_close_meeting_points",
        return_value=[meeting_points_mock.list_meeting_points[2]],
    )
    mocker.patch(
        "src.hubrdvmairie.services.search_time_slots.get_all_editors",
        return_value=[editor_mock.test_editor],
    )
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.search_slots_in_editor",
        return_value=([meeting_points_mock.meeting_points_with_time_slots[2]], None),
    )
    result, errors = await search_slots(
        longitude=2.357835,
        latitude=48.8717479,
        start_date=date.today(),
        end_date=date.today() + timedelta(days=90),
        radius_km=40,
    )
    assert len(result) == 1


@pytest.mark.asyncio
async def test_search_slots_no_close_meeting_points(mocker):
    mocker.patch(
        "src.hubrdvmairie.services.search_time_slots.get_all_meeting_points",
        return_value=meeting_points_mock.list_meeting_points,
    )
    mocker.patch(
        "src.hubrdvmairie.services.search_time_slots.search_close_meeting_points",
        return_value=[],
    )
    mocker.patch(
        "src.hubrdvmairie.services.search_time_slots.get_all_editors",
        return_value=[editor_mock.test_editor],
    )
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.search_slots_in_editor",
        return_value=([], None),
    )
    result, errors = await search_slots(
        longitude=2.357835,
        latitude=48.8717479,
        start_date=date.today(),
        end_date=date.today() + timedelta(days=90),
        radius_km=40,
    )
    assert len(result) == 0


@pytest.mark.asyncio
async def test_search_slots_no_time_slots(mocker):
    mocker.patch(
        "src.hubrdvmairie.services.search_time_slots.get_all_meeting_points",
        return_value=meeting_points_mock.list_meeting_points,
    )
    mocker.patch(
        "src.hubrdvmairie.services.search_time_slots.search_close_meeting_points",
        return_value=[meeting_points_mock.list_meeting_points[2]],
    )
    mocker.patch(
        "src.hubrdvmairie.services.search_time_slots.get_all_editors",
        return_value=[editor_mock.test_editor],
    )
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.search_slots_in_editor",
        return_value=([], None),
    )
    result, errors = await search_slots(
        longitude=2.357835,
        latitude=48.8717479,
        start_date=date.today(),
        end_date=date.today() + timedelta(days=90),
        radius_km=40,
    )
    assert len(result) == 0
