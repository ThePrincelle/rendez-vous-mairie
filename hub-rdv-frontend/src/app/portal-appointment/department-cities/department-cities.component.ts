import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Municipality } from '@shared/classes/municipality';
import { Department } from '@shared/classes/Department';
import { SearchCriteria } from '@shared/classes/search-criteria';
import { DeviceStateService } from '@shared/services/device-state.service';
import { PortalAppointmentService } from '@shared/services/portal-appointment.service';
import { DateUtils } from '@shared/utils/date';
import { Subscription } from 'rxjs';

@Component({
  selector: 'rdv-department-cities',
  templateUrl: './department-cities.component.html',
  styleUrls: ['./department-cities.component.scss'],
})
export class DepartmentCitiesComponent implements OnInit {
  zipcodeId = '';
  departmentCodes: Map<string, string> = new Map();
  municipalities?: Array<Municipality>;
  results: Map<string, Array<Municipality>> = new Map();
  department: any;
  citiesRange = 10;
  cities: Array<Municipality> | undefined;

  /**
   * indicator when search result is loading ( for spinner)
   * @type {boolean}
   */
  @Input() isLoadingSearchResult: boolean;
  /**
   * search criteria = form data + latitude and longitude
   * @type {SearchCriteria}
   */
  @Input() searchCriteriaIn?: SearchCriteria;
  /**
   * search criteria emitter to to the portal appointment component
   * @type {EventEmitter}
   */
  @Output() searchCriteria: EventEmitter<Array<SearchCriteria>>;
  @Output() latLong: EventEmitter<any>;
  dateMin: string;
  dateMax: string;
  isDesktop: boolean;
  errorMessage: any;
  private resizeSubscription?: Subscription;

  constructor(private portalAppointmentService: PortalAppointmentService, private deviceState: DeviceStateService) {
    this.isDesktop = !this.deviceState.isMobileResolution();
    this.searchCriteria = new EventEmitter<Array<SearchCriteria>>();

    this.latLong = new EventEmitter();
    this.isLoadingSearchResult = false;
    this.dateMin = DateUtils.getDefaultStartDate();
    this.dateMax = DateUtils.getEndDateMax(this.dateMin);
    this.errorMessage = {};
  }

  ngOnInit(): void {
    this.resizeSubscription = this.deviceState.onResize$.subscribe((mobile: any) => {
      this.isDesktop = !mobile;
    });
    let departmentCodesArray: any[] = [];
    Department.getAllDepartments().forEach((dept) => {
      departmentCodesArray.push([dept['code'], dept['name']]);
    });

    let departemntDecodedName = decodeURI(window.location.pathname).replace('/departement/', '');
    this.department = Department.findDepartmentByDecodedName(departemntDecodedName);
    this.portalAppointmentService.getMunicipalities().subscribe((res) => {
      this.municipalities = res;
      if (this.municipalities) {
        let x = this.municipalities.sort((a: Municipality, b: Municipality) => Number(a.zip_code) - Number(b.zip_code));
        for (let i = 0; i < x.length; i++) {
          if (x[i].zip_code && !this.results.get((x[i].zip_code + '').slice(0, 2)))
            this.results.set((x[i].zip_code + '').trim().slice(0, 2), new Array(x[i]));
          else {
            const found = this.results.get((x[i].zip_code + '').slice(0, 2))?.some((el) => el.city_name === x[i].city_name);
            if (!found) this.results.get((x[i].zip_code + '').slice(0, 2))?.push(x[i]);
          }
        }
        this.cities = this.results.get(this.department['code']);
      }
    });
  }

  getKeys() {
    if (this.department && this.department['code']) return this.results.get(this.department['code'])?.slice(0, this.citiesRange);
    return;
  }

  loadMore($event: Event) {
    if (this.cities) this.citiesRange = this.cities.length;
  }
}
