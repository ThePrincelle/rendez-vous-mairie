list_time_slots = [
    {
        "datetime": "2022-12-19T10:00Z",
        "callback_url": "http://www.ville-seclin.fr/rendez-vous/passeports?date=2022-12-19T10:00Z",
    },
    {
        "datetime": "2022-12-19T10:20Z",
        "callback_url": "http://www.ville-seclin.fr/rendez-vous/passeports?date=2022-12-19T10:20Z",
    },
    {
        "datetime": "2022-12-19T10:40Z",
        "callback_url": "http://www.ville-seclin.fr/rendez-vous/passeports?date=2022-12-19T10:40Z",
    },
    {
        "datetime": "2022-12-19T11:00Z",
        "callback_url": "http://www.ville-seclin.fr/rendez-vous/passeports?date=2022-12-19T11:00Z",
    },
]
