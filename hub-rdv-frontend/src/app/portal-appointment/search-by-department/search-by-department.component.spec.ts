import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { SearchByDepartmentComponent } from './search-by-department.component';

describe('SearchByDepartmentComponent', () => {
  let component: SearchByDepartmentComponent;
  let fixture: ComponentFixture<SearchByDepartmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchByDepartmentComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],

      providers: [HttpClient, FormBuilder],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchByDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
