import { fromEvent, Observable, Subject } from 'rxjs';
import { DeviceStateService } from '@shared/services/device-state.service';

export const fakeDeviceStateService: Pick<DeviceStateService, keyof DeviceStateService> = {
  isMobileResolution(): boolean {
    return false;
  },
  getScreen(): string {
    return 'LG';
  },
  resize(): Observable<Event> {
    return fromEvent(window, 'resize');
  },
  async isOnline() {
    return true;
  },
  get onResize$(): Observable<boolean> {
    return new Subject<boolean>().asObservable();
  },
};
