import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { WebsocketService } from './websocket.service';
import { HttpClient } from '@angular/common/http';

describe('WebsocketService', () => {
  let service: WebsocketService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpClient],
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(WebsocketService);
    jasmine.clock().install();
  });
  afterEach(() => {
    httpTestingController.verify();
    jasmine.clock().uninstall();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call create if connect is called', () => {
    let createSpy = spyOn<any>(service, 'create');
    service.connect();
    expect(createSpy).toHaveBeenCalled();
  });

  it('should return false if isWsOpen is called and ws undefined', () => {
    service.ws = undefined;
    expect(service.isWsOpen()).toBeFalsy();
  });

  it('should emit websocket state every 5 seconds when checkWebsocketConnexion is called', async () => {
    let emitSpy = spyOn(service.isConnected, 'emit');
    service.checkWebsocketConnexion();
    jasmine.clock().tick(3500);
    expect(emitSpy).toHaveBeenCalledTimes(0);
    jasmine.clock().tick(5500);
    expect(emitSpy).toHaveBeenCalledTimes(1);
  });
});
