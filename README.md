# Bienvenue sur le projet ANTS rendez-vous mairie  👋
Le projet rendez vous mairie est une application web qui vous permet de chercher et réserver des créneaux disponibles dans une mairie à pproximité pour déposer des dossiers de demande des titres.

Cette application va être utilisée par les citoyens francais.
## :pencil: Requirements

Vous trouverez ici les requirements du back-end:

[![python official](https://img.shields.io/static/v1?label=Python&color=FFE873&message=3.10.2&style=flat&logo=Python&labelColor=306998&logoColor=fff)](https://www.python.org/downloads/release/python-3102/)
[![fastAPI official](https://img.shields.io/static/v1?color=grey&message=0.79.0&style=flat&label=FastAPI&labelColor=000&logo=FastAPI&logoColor=fff)](https://fastapi.tiangolo.com/)
[![gunicorn official](https://img.shields.io/static/v1?color=grey&message=0.18.2&style=flat&label=Gunicorn&labelColor=138b44&logo=gunicorn&logoColor=fff)](https://www.uvicorn.org/)

Vous trouverez ici les requirements du frontend:

[![node official](https://img.shields.io/static/v1?label=Node&color=grey&message=16.16.0&style=flat&logo=node.js&labelColor=3c873a&logoColor=fff)](https://nodejs.org/en/)
[![npm official](https://img.shields.io/static/v1?label=npm&color=grey&message=8.11.0&style=flat&loge=npm&labelColor=1563FF)](https://docs.npmjs.com/)
[![angular official](https://img.shields.io/static/v1?color=grey&message=14.0.5&style=flat&label=Angular&labelColor=FF0000&logo=Angular&logoColor=fff)](https://angular.io/)

Pipeline

[![gitlab official](https://img.shields.io/static/v1?color=fca326&message=14.0.5&style=flat&label=Gitlab&labelColor=grey&logo=Gitlab&logoColor=fff)](https://gitlab.io/)
[![gitlab official](https://img.shields.io/static/v1?color=fca326&message=%&style=flat&label=JScoverage&labelColor=greylogo=Gitlab)](https://gitlab.io/)
[![gitlab official](https://img.shields.io/static/v1?color=fca326&message=%&style=flat&label=pythonCoverage&labelColor=greylogo=Gitlab)](https://gitlab.io/)



API Rest développée avec le framework FastAPI.

## Structure de projet
```
📦rendez-vous-mairie
 ┣ 📂.gitlab
 ┣ 📜.gitlab-ci.yml
 ┣ 📜.gitignore
 ┣ 📜requirements-dev.txt
 ┣ 📜.flake8
 ┗ 📂hub-rdv-backend
   ┣ 📜dispatch.yaml
   ┣ hub-rd-backend
   ┃ ┣ ...
   ┃ ┣ 📜requirements.txt
   ┃ ┣ 📜main.py
   ┃ ┗ 📜Procfile
   ┗ 📂hub-rdv-frontend
     ┣ ...
     ┣ 📜package.json
     ┣ 📜.prettierrc
     ┣ 📜.stylelintrc
     ┣ 📜.eslintrc.json
     ┣ 📜.prettierignore
     ┣ 📜server.js
     ┗ 📜Procfile
```

Please follow [KISS](https://en.wikipedia.org/wiki/KISS_principle) principles
- 📦rendez-vous-mairie : Le projet de base
- 📂.gitlab : gitlab ci et trucs de modèle
- 📜.gitlab-ci.yml : La configuration gitlab ci
- 📂hub-rdv-backend: backend en Python 
- 📂hub-rdv-frontend: application frontale dans angular 


## 🔧 Development
Install dependencies and configuration

### Backend

```bash
# Allez au dossier hub-rdv-backend
cd hub-rdv-backend
# install pipenv avec pip
python -m pip install pipenv
# activate virtual environment with
python -m pipenv shell
# install python dependencies and dev dependencies
python -m pipenv install --dev
```

### Frontend

```bash
# Allez au dossier hub-rdv-frontend
cd hu-rdv-frontend
# Installer les dépendances de node
npm install
```

### Run it

#### Run front in local
```bash
# Allez au dossier hub-rdv-frontend
cd hu-rdv-frontend
# Installer les dépendances de node
npm start
```
#### Run backend
- Ajouter le fichier .env au racine de projet backned
```bash
# Allez au dossier hub-rdv-frontend
cd hu-rdv-backend
# Créer la fichier .env
touch .env
# copy la configuration suivante

# Noms de domaines white-listés
ALLOW_ORIGINS=["https://rendezvouspasseport.ants.gouv.fr", "https://rendez-vous.france-identite.fr"]
# * Configuration d'une alerte générale sur le site
# Couleur de l'alerte - couleurs possibles : ['warning', 'info', 'error', 'success']
ANNOUNCEMENT_ALERT_LEVEL=warning
# Titre de l'alerte
ANNOUNCEMENT_TITLE=
# Description de l'alerte
ANNOUNCEMENT_DESCRIPTION=Attention, une campagne de hameçonnage est en cours. Nous mettons en place les mesures nécessaires.
# Les tokens autorisés à communiquer avec l'API
AUTH_TOKENS=["testFA12KJDN42"]
# l'environnement : dev/prod
ENVIRONMENT=prod
# Mot de passe de la liste des mairies offline
EXCEL_PASSWORD=secret
# Identifiant Google 
GOOGLE_SITE_VERIFICATION=XXXXXXXXXXXXXXXXXXXXXXXXXXX
HOST=0.0.0.0
LOG_LEVEL=INFO
# Pour générer des éditeurs fictifs
MOCK_EDITORS=False
# Pour simuler les réponses
MOCK_EMPTY_RESPONSE=False
# Token pour commiuniquer avec l'API doublons
OPTIMISATION_API_TOKEN=secret
# URL pour commiuniquer avec l'API doublons
OPTIMISATION_API_URL=https://rdvmairie-optimisation-dev.osc-secnum-fr1.scalingo.io
# Variable dédié à Scalingo
PROJECT_DIR=hub-rdv-backend
# Active/Désactive le rédmarrage automatique du serveur lorsqu'il y'a un changement dans le code
UVICORN_RELOAD=False
# Nombre de workers uvicorn
WORKERS=4
# Les tokens des APIs d'éditeurs
agglocompiegne_auth_token=secret
aggloroyan_auth_token=secret
citopia_auth_token=secret
esii_auth_token=secret
infolocal_auth_token=secret
kembs_auth_token=secret
numesia_auth_token=secret
rdv360_auth_token=secret
smartagenda_auth_token=secret
solocal_auth_token=secret
soultzsousforet_auth_token=secret
synbird_auth_token=secret
troov_auth_token=secret
un23mairie_auth_token=secret
ypok_auth_token=secret

# Lancer le backend
python main.py
```
## :warning:️ testing
Pour exécuter des tests backend

```shell
# Allez au dossier hub-rdv-backend
 cd hub-rdv-backend
 # Exécuter la cmd
 python -m pipenv test

 pytest --cov . --cov-report html
```
#### test frontend
```shell
# Allez au dossier hub-rdv-frontend
 cd hub-rdv-frontend
# si vous $ête sur windows n'oblier pas à exporter le variable CHROME_BIN
export CHROME_BIN="C:\Program Files\Google\Chrome\Application\chrome.exe"
# Exécuter la cmd
 npm test
# install python dependencies and dev dependencies
 ng test --no-watch --code-coverage
```

## :warning:️ linting
### backend
Pour garder le format du code, assurez-vous d'installer les fichiers `requirements-dev.txt`.

```shell
# Allez au dossier hub-rdv-backend
 flake8 hub-rdv-backend
 cd hub-rdv-backend
 # Exécuter
 invoke format lint

 # en cas de permission denied
 python -m pipenv run format-black
 python -m pipenv run format-isort
 python -m pipenv run format-autoflake
 hub-rdv-backend
```
pour verifier si le format et le style et bon
```shell
# Allez au dossier hub-rdv-backend
 cd hub-rdv-backend
 # Exécuter
 python -m pipenv run lint-black
 python -m pipenv run lint-isort
 python -m pipenv run lint-autoflake
```
### Frontend
```shell
 cd hub-rdv-frontend
 # pour corriger le lint et le style automatiquement
 npm run format
 # pour vérifier le lint et le style
  npm run lint
```
## URLs of the APP:

|environnement |url front  |url swagger  |url api  |
|-----------|------|------|------|
|local| http://127.0.0.1:4200|http://127.0.0.1:8080/docs|http://127.0.0.1:8081/api |
|ppd| https://ppd.rendezvouspasseport.ants.gouv.fr/|http://127.0.0.1:8080/docs|https://ppd.rendezvouspasseport.ants.gouv.fr/|http://127.0.0.1:8080/api |
|production| https://hub-rdv-frontend.osc-fr1.scalingo.io/|https://rendez-vous-api.france-identite.fr/docs| https://rendez-vous-api.france-identite.fr/api|



## Publishing package on PyPI
```shell
 cd hub-rdv-backend
 # pour créer les fichiers 
 python -m build
 # pour uploader les fichiers vers PyPI
 python -m twine dist/*
```