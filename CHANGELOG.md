# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.23.1](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.23.0...2.23.1) (2023-07-12)


### Features

* add some unit tests ([d87ba78](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d87ba78c9947952d2a4b54da0a764a5e1509c5f5))
* group-overseas-cities-in-one-block ([70da238](https://gitlab.com/france-identite/rendez-vous-mairie/commit/70da238d1c2f053b0426b46cfbd4ed61f083162c))
* Rajouter l'éditeur Anaximandre ([7dacaaa](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7dacaaabd1b8ca42b34f6c983e3785eaf22fd859))
* update ati-controler and id of cgu component ([42412ca](https://gitlab.com/france-identite/rendez-vous-mairie/commit/42412cab0c8f69dd0d85e5b9a436049d3fbbc739))
* update Get Available Time Slots description ([210d7f9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/210d7f95ce451e8ecc7d46d61109290b287e35cc))


### Bug Fixes

* affiche tous les resultats disponibles en mode mobile comme dans anciennes version ([7b50b03](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7b50b03cb07ee3a7ed6c5e8d8a5d06f31b8fed91))
* Change le site web d'Alata ([7ccee35](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7ccee35061eef147cc4c42675753cd5f495961f0))
* Corriger l'URL de Neuville + mettre à jour le sitemap ([bcf5f28](https://gitlab.com/france-identite/rendez-vous-mairie/commit/bcf5f28669f87c8614455f0baf9cc9f0130e3036))
* Corriger le bug padding left right après un changement d'onglet ([54c10f9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/54c10f9f60592dae623aa6074e98cc2b1e3b4cb8))
* css clean et résolution soucis dark mode ([ec2e7fc](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ec2e7fc2d1fa02383149737bf6067414408c2c97))
* delete useless buttons and improve accessibility ([6579b84](https://gitlab.com/france-identite/rendez-vous-mairie/commit/6579b84cb9bea0cf177b6737e371d5d8a9919db6))
* Discard slot schedule changes for Bussy && Creney-près Troyes ([65e6bea](https://gitlab.com/france-identite/rendez-vous-mairie/commit/65e6beac41d458fe4238ff8292d2e5fb0354693f))
* filter municipalities by canonized name ([bfd0a50](https://gitlab.com/france-identite/rendez-vous-mairie/commit/bfd0a50db126afd5a963be0734473e2e303dde3a))
* Ignorer les erreurs de searchApplicationIds pour le status d'un éditeur ([d967dd7](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d967dd7b08b38312213669bff732a462aea33ce8))
* Nouvelles corrections du fichier des mairies offline v6 ([b39effd](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b39effd4b5c83d0f1d30357f0a36bbf21d07fd81))
* search by city text for SEO ([95dcf60](https://gitlab.com/france-identite/rendez-vous-mairie/commit/95dcf60ae25deb06e790c1d5f9650df8daab4ded))
* select-number-id ([3d09c33](https://gitlab.com/france-identite/rendez-vous-mairie/commit/3d09c3379389dbefc3b2a5ee67b6557f3f5610f9))
* tabs end index ([68188fe](https://gitlab.com/france-identite/rendez-vous-mairie/commit/68188feaf69cf0953c2d3a5484fbc6af67224373))

## [2.22.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.21.0...2.22.0) (2023-06-29)


### Features

* Ajouter l'éditeur Gallia et corriger Synapse ([028b4bf](https://gitlab.com/france-identite/rendez-vous-mairie/commit/028b4bfef52daa5909b3fec151fe79776a6fb9bc))
* Rajouter les mairies de Plérin et Aurillac ([f416a1e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f416a1e0aca1c7981ffb93bfd00f22f99cc0bfc0))


### Bug Fixes

* application id link on mobile and other issues ([1e1b5ef](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1e1b5efc21904bdcda165656ade75151eedf8904))
* deplacement import dsfr ([a37873f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a37873f3095e90e84b25ba4db0fb1e32ceea1777))
* Gérer les retours de mauvais format de /availableTimeSlots ([51b8f08](https://gitlab.com/france-identite/rendez-vous-mairie/commit/51b8f084c6b1521cb46f33559da39af45bda5b6c))

## [2.21.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.20.0...2.21.0) (2023-06-28)


### Features

* ajout espacerendezvous 1 et 2 ([26039a7](https://gitlab.com/france-identite/rendez-vous-mairie/commit/26039a7d3c1c8976c0fb991a669ec55bd98c6184))
* Liens dans le header et fil d'Ariane sur toutes les pages + refonte CGU ([9a3aea4](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9a3aea4e4f6e2e3894e157cf6475272d06baeaf8))
* raccordement Pont st esprit et st etienne du rouvray ([100eb63](https://gitlab.com/france-identite/rendez-vous-mairie/commit/100eb63837ede45b7518ee38d4c83fd43df83508))
* raccordements pont_saint_esprit, saint_etienne_du_rouvray, boisseuil ([9e84d66](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9e84d66dfa5f2924bed389113b0ecc46f10d6370))
* Rajouteur la mairie de Boisseuil ([795a9db](https://gitlab.com/france-identite/rendez-vous-mairie/commit/795a9dbbe13375564e166781568d31aa04b48fad))


### Bug Fixes

* aria-current issue on header ([3f1b3ef](https://gitlab.com/france-identite/rendez-vous-mairie/commit/3f1b3efdf923171496c3509899e46327a5094a5b))
* Arrow key press multi triggering and update 'site internet' button modal message ([2a4e3e8](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2a4e3e8c1d7ba21b4156c39b6329d5f7ca2696ed))
* Nouvelles corrections du fichier Excel des mairies offline ([1ee7d6e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1ee7d6e5db369b82a63837b48d5a8381302b2e7c))
* pages title and offlineMunicipalities issues ([221ac06](https://gitlab.com/france-identite/rendez-vous-mairie/commit/221ac062f1dc3957e35a7aaef11742726795e7b5))

## [2.20.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.19.0...2.20.0) (2023-06-23)


### Features

* Rajouter l'éditeur Mobminder ([e37bdd6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e37bdd60dd22471fc06163c84e63224f9d6abc6f))
* Rajouter l'éditeur Synapse ([0028203](https://gitlab.com/france-identite/rendez-vous-mairie/commit/00282035feabef36df1493fea2b8f54394bbb853))
* Rajouter la mairie de Lorient ([0a37a16](https://gitlab.com/france-identite/rendez-vous-mairie/commit/0a37a16d180c4d18035a6836934bbb0c0c234a46))


### Bug Fixes

* accessibility message ([e8f4a1c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e8f4a1c29909b124410c63fd4dc4c89c8fe17fee))
* delete useless css and some adjustments ([93c600e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/93c600ed6e186028292327b881d62091f68f0100))
* update dev with lost commits ([abbd8b8](https://gitlab.com/france-identite/rendez-vous-mairie/commit/abbd8b8b28d92c7d2bcea3dc0213bd5133d66df2))

## [2.19.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.18.0...2.19.0) (2023-06-21)


### Features

* accessibility focus ([75082ab](https://gitlab.com/france-identite/rendez-vous-mairie/commit/75082abf8f6995deab0086abb95232badfe40f15))
* Activer l'éditeur Créasit ([402e453](https://gitlab.com/france-identite/rendez-vous-mairie/commit/402e453fa4b1cd20f343ef8cafb97b8af702695c))
* Rajouter la mairie de Pecq + Gérer les mairies sans nom de ville ([def2bc0](https://gitlab.com/france-identite/rendez-vous-mairie/commit/def2bc0277a1f4f46231b1985ced9573804bcbbd))
* Rajouter la mairie de Wittisheim ([0026637](https://gitlab.com/france-identite/rendez-vous-mairie/commit/002663730f66a94280e1a8855f70ff2c63d20b9d))
* tabs accessibility on Arrow Key left and right press ([b792967](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b792967dfb348fe40f21e2cba770801051654b69))


### Bug Fixes

* Corriger le code postal de Saint-Raphaël ([b594a8e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b594a8e8dd1c5513696b84173f77b2d8e2680633))

## [2.18.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.18.0-0...2.18.0) (2023-06-21)


### Features

* update url numesia ([a42a618](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a42a6183a34774d8814fc84fa3809fd1e819fe66))


### Bug Fixes

* mobile offline municipalities title ([22fb0da](https://gitlab.com/france-identite/rendez-vous-mairie/commit/22fb0da902c41b3765ac2d4daac36ffde2098ab0))

## [2.18.0-0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.17.1...2.18.0-0) (2023-06-14)


### Features

* add-a-specific-tab-for-offline-municipalities ([2bfe860](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2bfe860fb466f86dde853561ca2e0ea73e4b51cd))
* rajouter l'éditeur Entrouver ([5a15877](https://gitlab.com/france-identite/rendez-vous-mairie/commit/5a15877f32d79951ce15355bf9f16dd03e63ad0e))
* Rajouter l'éditeur GIE Convergence ([f92d0aa](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f92d0aae5f63f2bb698f6160b59c8708e5db87a5))


### Bug Fixes

* Corrections fichier excel - vague 4 ([879be7a](https://gitlab.com/france-identite/rendez-vous-mairie/commit/879be7a59aca11cd6718e668120dbb487da6d6c1))
* update cities/departments page description ([846e42d](https://gitlab.com/france-identite/rendez-vous-mairie/commit/846e42d867b8f42a4514233a81cdc9b192a01ca8))
* update error message when websocket is reconnected ([4d0a13b](https://gitlab.com/france-identite/rendez-vous-mairie/commit/4d0a13b98686dc15b80679f2296966e1c2a37b9b))
* update sitemap file with new cities ([f9ab7a9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f9ab7a984bf6be47fe379b1513e60b482599883f))

### [2.17.1](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.17.0...2.17.1) (2023-06-09)


### Features

* intégrer l'éditeur ESII on premise ([a33a228](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a33a228c0378f8dff84b76fe49c6e39934225467))


### Bug Fixes

* corrections fichier excel vague 3 ([46fac14](https://gitlab.com/france-identite/rendez-vous-mairie/commit/46fac14dba52e0672e8f8810d69c79ee27a0504b))
* enter click for page ville ([1a09b07](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1a09b07e79cddefece6c9fbed86aedca12ed9dd5))
* some department logos issues ([0e8eb6c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/0e8eb6c0c82a1f3e236d1bea92eabeb335940f28))

## [2.17.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.16.0...2.17.0) (2023-06-08)


### Features

* add message error to handle httperror response ([feaef02](https://gitlab.com/france-identite/rendez-vous-mairie/commit/feaef02f8d40d751767fa205bfebfee17d4bd2d4))
* Ajouter l'éditeur Creasit ([88189b5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/88189b57c5ad71c089f150aa57b10db52845c7de))
* intégration de l'éditeur Vernalis ([923ec60](https://gitlab.com/france-identite/rendez-vous-mairie/commit/923ec6035bcc3b1e3644189601bcf382ac01e64a))
* mise à jour dernière version dsfr ([ca7769a](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ca7769a4c3d9e05695a8ad0e2b11beaf82d37b97))
* modification title sur mairie hors ligne [#450](https://gitlab.com/france-identite/rendez-vous-mairie/issues/450) ([3fd698e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/3fd698e7bdeb0e64c35acc62b13357ddf8d31528))
* set-priority-between-slots-and-offline-meeting-points ([7ad90e1](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7ad90e195aa891852fe267fabdd9f6e01f94947f))
* smart search for *cities ([245a47e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/245a47ec9a81b335f2db293c7f0a0c25075f5c5f))
* update message and fix responsive view ([e0bc4bc](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e0bc4bc3edb4852fb57b3509714499dce5f33867))


### Bug Fixes

* add offline municipalities to search-by-department results ([9121866](https://gitlab.com/france-identite/rendez-vous-mairie/commit/91218661571e79075c6cdaf1aa50b2bf93ed454a))
* adjustment-in-predemande-page ([9ad86f3](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9ad86f3cb3e91887ef14ee266f3039fcdddd3221))
* alignment-issues-in-the-pre-demande-page ([bb37981](https://gitlab.com/france-identite/rendez-vous-mairie/commit/bb37981acf98a6bf49586a4954cf31fee52fcf87))
* button title attribute ([178f416](https://gitlab.com/france-identite/rendez-vous-mairie/commit/178f416d923b0213b4bfa82ba1d4efc06670a71a))
* change-offline-meeting-point-end-location ([3c3d9f5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/3c3d9f5bbdaa3889ed862e6e11ea10ff5a84b5f6))
* cities-and-departments-after-smart-search ([c33b544](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c33b54409f7853454528feb00b1f92d0c069435c))
* city search page design issues ([03e5186](https://gitlab.com/france-identite/rendez-vous-mairie/commit/03e5186de03175bbda557500d303026281af5818))
* delete-useless-code ([5bfc78b](https://gitlab.com/france-identite/rendez-vous-mairie/commit/5bfc78b9735bf20d910be15314ac1bc8e792b66d))
* duplicate-appointments-in-application-ids ([c18e7fb](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c18e7fb11a5f6b09905bfda197a50154eb810131))
* Filtrer les mairies offline par nom et adresse aussi ([2537d57](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2537d57d383b1813b00505239ffd884657c994a2))
* form inputs visibility on-all screens ([191e0c9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/191e0c942cb9c4b213684732da8d9984764d0728))
* handle-duplicated-code ([904d07c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/904d07c3d3ec83e6053c573a5f7f5f18e687bb19))
* issues after refactoring ([fa41898](https://gitlab.com/france-identite/rendez-vous-mairie/commit/fa418985da0cc9d490b61d31248a538e109243c2))
* issues in dev ([6163805](https://gitlab.com/france-identite/rendez-vous-mairie/commit/6163805ed97bc5da24f42481ed87820b6bf090c4))
* Mettre à jour le fichier excel des mairies offline ([19e1fe0](https://gitlab.com/france-identite/rendez-vous-mairie/commit/19e1fe0ef241da297f37b0ed2afa406aef05be5d))
* Mettre à jour les variables d'env dans le README avec plus d'explication ([6425754](https://gitlab.com/france-identite/rendez-vous-mairie/commit/64257545dead355ec40eadfacf3c809a3621e58c))
* modal-code-redundancy ([6aada21](https://gitlab.com/france-identite/rendez-vous-mairie/commit/6aada21a5c2f7f2e1d8e76d561ddf23a9a413520))
* move-DepartmentCodes-object-to-a-class ([5821499](https://gitlab.com/france-identite/rendez-vous-mairie/commit/582149940777fa9c3e5703f7cbf858374d0dea0b))
* personaliser le titre pour la page villes et département ([c75cbb5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c75cbb55a63e688197e1a37a71d992c7c5f5c527))
* phone number in sort by distance and other issues ([81a9787](https://gitlab.com/france-identite/rendez-vous-mairie/commit/81a978719b961bda1b12bce268163f84e01f1d3a))
* réserver-button-title-in-sort-by-distance ([216ee07](https://gitlab.com/france-identite/rendez-vous-mairie/commit/216ee07c23c4c542a1cdff9a318a7b9c60fe001b))
* retrait flèche sur le liens déterritorialisation ([afb80b1](https://gitlab.com/france-identite/rendez-vous-mairie/commit/afb80b1a0010d66ddfc0f458cddbcc2889ae0464))
* slots-default-logo-mobile-issue ([4bf8a19](https://gitlab.com/france-identite/rendez-vous-mairie/commit/4bf8a19af878751441c634c932b3dce987e76443))
* smart-search-when-returned-slots-expired ([52c16e7](https://gitlab.com/france-identite/rendez-vous-mairie/commit/52c16e77cb324a3f02a313da540921a2ec41a53a))
* update meeting points data ([f213d53](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f213d5365e7ca46d07202ecc29751f5141e787d1))
* vider l'attribut alt de la balise img et ajouter l'attribut title pour decrire l'image ([a5c1b75](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a5c1b75661bba16d7a2c68bf4219c59a29665273))

## [2.16.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.15.0...2.16.0) (2023-05-21)


### Features

* add style for mairie raccordé ([2fe0e37](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2fe0e37b29a5ad57a8c7edbad5ba423524607a65))
* Chercher les rendez-vous dans l'api optimisation en plus de la recherche chez les éditeurs et fusionner les résultats ([887dcd3](https://gitlab.com/france-identite/rendez-vous-mairie/commit/887dcd3dc7e4b76f6e899ed5dd985e522a204a21))
* Si des rendez-vous de searchApplicationIds n'existent pas dans l'api optimisation on fait une mise à jour au nom de RDVMairie ([8047edd](https://gitlab.com/france-identite/rendez-vous-mairie/commit/8047edd3123e4717f36595072294b94fee825322))
* update messagte offline ([88189b5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/88189b53001585c845d923dea174c2f048aa8b59))
* update modal message ([2ad9730](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2ad97304cece3643ac5fe7d77a4e07e6d7abd942))


### Bug Fixes

* disable-more-settings ([f451528](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f45152889f9e565876a88ac3e4804fa343a0cb82))
* install dateutil module ([8d4adba](https://gitlab.com/france-identite/rendez-vous-mairie/commit/8d4adbab0e7f6d355e21dd53583217d07d142c11))
* offline municipalities by location ([8d5ade4](https://gitlab.com/france-identite/rendez-vous-mairie/commit/8d5ade48909f356d777306dd883fb9ddf8e2d2ea))
* offline municipalities by location ([1ecc587](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1ecc5877861c5020e686de2f6b1a7bbd6cc127e8))
* order-between-slots-and-municipalities-offline ([1baeca9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1baeca994850ef6dac0fe3124ab70fdbf4e79e7b))
* paragaraphe répété dasn la page en savoir plus ([3f1e8e1](https://gitlab.com/france-identite/rendez-vous-mairie/commit/3f1e8e1e4659eaf7216e6fd4a55e164c12360be5))
* responsive+ add new style for offline ([398c8a6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/398c8a65b18f2f6b4212e1d172d5266170c0ba5a))
* settle 80 km search ([31dcb18](https://gitlab.com/france-identite/rendez-vous-mairie/commit/31dcb18400f6a7df16ae87dfffdbba48fadea6d7))

## [2.15.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.14.0...2.15.0) (2023-05-14)


### Features

* Filtrer les mairies offline par leur code postal + latitude + longitude ([04c51c2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/04c51c2d707d564f10a3513ef90a26bded9b9e2f))


### Bug Fixes

* Accessibility rate ([1ea8757](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1ea875756e6774a4fc6cb83356abc9b953a03908))
* Changer l'ordre des routes pour mettre celles des éditeurs en premier ([0ddbfc6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/0ddbfc6d291cbb8922b913b7729579129d471de7))
* delete file ([d4a77c0](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d4a77c09f19df3c3e37ed3d5373615fd77dae82b))
* distance from any position in template mobile ([f789366](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f7893666761234df1efb5af47dd03f74e1d3af8a))
* fix issues ([7e1b520](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7e1b52092392a72b5d4f05fdb8e6f1c7fac7364d))
* fix/issue-offline-meeting-point-name ([d1bb26d](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d1bb26ddaa36a92218ecc4aca525f074660fb058))
* insert the good excel file ([b17a88b](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b17a88b03a8eb5102f30b730a022804519c9634c))
* offline-meeting-points-issues ([5be54a9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/5be54a98dc302dc48700ea2adabe034ed92448bb))
* responsive slots+ add icon telephone ([f1036c9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f1036c94414c7c92b653ca031bec74454c75b17f))

## [2.14.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.13.0...2.14.0) (2023-05-07)


### Features

* change maquette ville ([963e0ab](https://gitlab.com/france-identite/rendez-vous-mairie/commit/963e0abab8e94b5e73c81a923166c8f366ce7724))
* update maquete departememnt + harminsie majuscule minuscule ([c63173d](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c63173dabd244170524ed1983650baca51cbc533))


### Bug Fixes

* add offline meeting point ([3c6e12e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/3c6e12e15534ceba47d7a84acc1cfd26bc0f62cd))

## [2.13.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.12.2...2.13.0) (2023-04-27)


### Features

* ajouter validator des champs date ([bea98a0](https://gitlab.com/france-identite/rendez-vous-mairie/commit/bea98a0812af8ae064992373cfeee1f041bbbaaf))
* Rajouter les 2 mairies Soultz-sous-foret et Kembs ([0aeaf93](https://gitlab.com/france-identite/rendez-vous-mairie/commit/0aeaf93c634037f0ea69755b597c5096a8920cb1))


### Bug Fixes

* add balise and fix title h2 ([95e7277](https://gitlab.com/france-identite/rendez-vous-mairie/commit/95e72773d415fcf0cccc75112b1bd8f914b09db4))
* add balise and fix title h2 ([38369a3](https://gitlab.com/france-identite/rendez-vous-mairie/commit/38369a3d56d673e51fedc13b10639457ed718ff4))
* add-déterritorialisation-link-in-mobile ([0944a8e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/0944a8efefdcc9d5d0a700671d096435b3388573))
* ajouter label à composonat autocomplete ([0583224](https://gitlab.com/france-identite/rendez-vous-mairie/commit/058322446d51f36cc378e3815de9f91afcd43b98))
* button reserve ([428d45f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/428d45f2f83b25e3ddf6403adcbde961fe44c2ed))
* enlever espace de addresse ([05d73b5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/05d73b535d23e90e663d45005dbe86aee299c0f0))
* enlever espace de addresse ([27cb0c5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/27cb0c54d347489101274903fe59791728ac24f6))
* enlever espace de addresse ([3d673c7](https://gitlab.com/france-identite/rendez-vous-mairie/commit/3d673c7a960c6137a6a49d0dffd7f7a93bf541df))
* la tabulation passe à longlet suivant et remplacer ce comportement par de fleche ([9d7f655](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9d7f655929e7e266c361938616101222bb92e28b))
* onDestroy ([ef4144e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ef4144eb61dd10f61fd22c16fd6c53df2fa174de))
* Ordre alphabétique des noms d’éditeurs dans la page « Status » ([1cf1764](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1cf1764362a6b4cc86bc10c581a8abb30fee52e9))
* placeholder contrast ([23ca19d](https://gitlab.com/france-identite/rendez-vous-mairie/commit/23ca19d6236937e5bc74df418cfb0b36657fcc46))
* savoir-plus-text-update ([a74d1c6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a74d1c63ffc59cb0e2af7fa0fd120276f7ff6e88))
* template distance ([5b1c1ae](https://gitlab.com/france-identite/rendez-vous-mairie/commit/5b1c1ae9c325d63b17315f144eafa11d47f6f08d))

### [2.12.2](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.12.1...2.12.2) (2023-04-14)


### Features

* ajouter h1 balise pour le tire page predemande ([47129f7](https://gitlab.com/france-identite/rendez-vous-mairie/commit/47129f7cadda74f241a19b8e59859936ba3b2159))
* change message no slots ([c894a1d](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c894a1dbb0635297e5e727792f4876d443dacca7))


### Bug Fixes

* balise p ([b02a9d1](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b02a9d1cf6c0e1cc0076eae607f25ab26ba1211c))
* button information and add title to reserve button ([852e549](https://gitlab.com/france-identite/rendez-vous-mairie/commit/852e549dd826789022482bba2889584ddd317ed2))
* Mettre les alertes dans une balise <p> ([8b08704](https://gitlab.com/france-identite/rendez-vous-mairie/commit/8b0870456d78736a6b01adf2f7de009e6ae25e29))
* remplacer le code postal d'Evron ([f30b9fe](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f30b9fefb6e69813540127fa5cc9c2986e9ed9b8))
* Remplacer temporairement les codes postales du BAN de quelques villes ([9093596](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9093596c8da24db6624819905d86279d0298b1e0))
* reserver button onclick ([d266e21](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d266e21ddbe6331c1943ce6fdebba8da657c94b3))
* slots table does not have good header declared ([5863299](https://gitlab.com/france-identite/rendez-vous-mairie/commit/5863299efc319bfb7a4f3adc76366100203e1576))

### [2.12.1](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.12.0...2.12.1) (2023-04-04)


### Features

* Activer l'éditeur Troov ([924456f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/924456f30faee6d6fa7a664dd771bb9e60617f43))


### Bug Fixes

* city page title ([f0338c3](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f0338c3b5044515dbb0e15dc16bc47e2ccce307c))
* fix issues ([d839f1a](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d839f1a7a735aa877d8795c1de11bc3c4861a0bf))
* reserve button mobile ([666ee41](https://gitlab.com/france-identite/rendez-vous-mairie/commit/666ee41b53d2fc4e1f7f02598d46a583434e446c))

## [2.12.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.11.0...2.12.0) (2023-03-30)


### Features

* add limiter to slots table and display offline municipality when there is no municpality ([1371abd](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1371abd6729b4253d92839ca61c8634bba143b75))
* add page information deterritorialisation ([7c6a514](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7c6a514f96933b76e45549920ed20495e0d85f24))
* Gray-the-page-and-hide-spinner-when-position-is-deactivated ([c460cb1](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c460cb138aba2baafea8fbb7bf93d731bc8e7ead))
* Nouvelle version du fichier des mairies offline ([9da05b5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9da05b520ef3c9d40c9ccb2e2cdaaf25b4183398))
* retirer-le-motif-de-retrait ([6b08288](https://gitlab.com/france-identite/rendez-vous-mairie/commit/6b0828878916159898bf0bce1df1d75f15f4b424))


### Bug Fixes

* expend-cities-on-departement-click-in-cities-page-angular ([a54e545](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a54e545b23329d0c7d2f2bad8f0db24652138600))

## [2.11.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.10.0...2.11.0) (2023-03-24)


### Features

* display municipalities offline mobile version ([1ffd276](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1ffd276d770f5c273167384758487e9ecad413bc))
* display-mairie-offline-desktop ([e87d381](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e87d3816595c4f4b6777d830cc069443ddc4895d))
* migrate template creneaux mobile ([81e6984](https://gitlab.com/france-identite/rendez-vous-mairie/commit/81e6984e79c78de0d6b88ef4983f2ea187996cff))


### Bug Fixes

* Rajouter le mot 'aux' aux mots exclus de la majiscule ([ca3f4e2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ca3f4e238401757b247ded341927cae3ba80e8b4))
* Transformer les noms de ville envoyés par les éditeurs en Majiscule première lettre et miniscule le reste ([6af7573](https://gitlab.com/france-identite/rendez-vous-mairie/commit/6af7573140c150fe3c22e2db7552759c742b167f))

## [2.10.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.9.0...2.10.0) (2023-03-15)


### Features

* add autocomplete to page cities ([c507cec](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c507cece1fc31377ae772e6399fd87de86c48ffb))
* Ajouter l'éditeur aggro royan ([79d8c03](https://gitlab.com/france-identite/rendez-vous-mairie/commit/79d8c0326dfca18a743fe2680cad12f3402a9fee))
* Ajouter la mairie Agglo Compiegne en tant qu'éditeur ([008518e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/008518ee457774d1b3907730e80608a0e5eff697))
* api pour chercher les mairies proches et ne sont pas connectées à un éditeur ([15ca1d0](https://gitlab.com/france-identite/rendez-vous-mairie/commit/15ca1d0302686eaec6038c7c9950d512a5b0253c))
* Chercher les mairies offline + exclure celles qui sont raccordées ([0153eeb](https://gitlab.com/france-identite/rendez-vous-mairie/commit/0153eeb8121748d9272ef6e8c9e4cfaa73f99d96))
* Les meeting points sont dans un fichier Excel protégé par un mot de passe ([f0dfda2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f0dfda2c2d0f07f2547f59121bd809a0460ca96f))
* URLs de départements en ASCII + sitemap des départements ([8493aad](https://gitlab.com/france-identite/rendez-vous-mairie/commit/8493aad51551f2844994018a6f1cdb45c33819fa))


### Bug Fixes

* désindexer la page pré-demande avec num=111111111 + rajouter un lien... ([a0f681f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a0f681feb745ef48df73a76816bb8aa7b1342c7a))
* éviter la dépendance circulaire entre editor.py et utils.py ([f7bd2ab](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f7bd2ab3a3368b549f760c602525ff9d3cf8ff4c))
* Faire passer les urls de callback et logo à travers le DomSanitizer d'Angular ([dff8257](https://gitlab.com/france-identite/rendez-vous-mairie/commit/dff82577a1a8164c581e19a2657dc76ff0a39889))
* footer version ([847e4d1](https://gitlab.com/france-identite/rendez-vous-mairie/commit/847e4d1c7bdc8e69b9df923e8366b361de234d50))
* get offline mairies ([73f13b3](https://gitlab.com/france-identite/rendez-vous-mairie/commit/73f13b363066434724f72346cb0c95a65c3c324f))

## [2.9.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.8.0...2.9.0) (2023-02-10)


### Features

* Un service pour insérer la balise link canonical ([3c6bf3f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/3c6bf3f5d04a51174bfe2e233c12c695f4b88f0b))


### Bug Fixes

* is-online install ([08559d9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/08559d9fd3b6a4ab18ac7144d95303e1340d01f7))

## [2.8.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.7.1...2.8.0) (2023-02-01)


### Features

* add docstring to somme function and file ([2a8580d](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2a8580d4aa215f2c7de6cb90e1632a58c6b4ada9))
* Ajouter l'éditeur Info Locale ([263b96a](https://gitlab.com/france-identite/rendez-vous-mairie/commit/263b96a0cda5043fb8f13bdb99748b6d205cb6a5))
* Ajouter le statut du backend dans la page /status ([27aac39](https://gitlab.com/france-identite/rendez-vous-mairie/commit/27aac394fc8a7c8cba39eb01533ecf66f8415f66))
* Ajouter le statut du backend dans la page /status ([92ea32e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/92ea32efa474acebfde9c230a14865c83554fe0f))
* Désactiver l'éditeur Troov ([925f6e2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/925f6e2413b080ba60e029e6b98a32faa48acbe5))
* Relancer /getManagedMeetingPoints dans un nouveau threadi si y'a une erreur ([450ecb7](https://gitlab.com/france-identite/rendez-vous-mairie/commit/450ecb7d9b196047cb6cb718d0e819a36baa6163))


### Bug Fixes

* La localisation du fichier robots.txt a changé dans le ssr ([d67feb0](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d67feb0e7f491b315570bc35d69f2af40c96cafb))
* specify the disconnection error message ([f631878](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f6318788b388bb4bdb5a86a9c7061facea5a8df6))

### [2.7.2](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.7.1...2.7.2) (2023-02-01)

### [2.7.1](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.7.0...2.7.1) (2023-01-20)

## [2.7.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.7.0-0...2.7.0) (2023-01-20)


### Features

* Recherche par mois seulement chez les éditeurs ([0ab6050](https://gitlab.com/france-identite/rendez-vous-mairie/commit/0ab605029eda7fc29b313a5039329ea58c532315))


### Bug Fixes

* Accepter les lettres spéciales dans la recherche ( exemple œ ) ([d95325e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d95325ea4c0fcc3cd3e25461802793c4e549d5cc))
* expired time slots ([86487ef](https://gitlab.com/france-identite/rendez-vous-mairie/commit/86487eff12a1bf450531d3f79b1952f2b61efbc6))
* Traiter le décallage horaire dans pré-demande de la même manière qu'on a traité les time slots ([b33e74d](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b33e74d1bedce737ff10d0fcdfb7e1195f17bf06))

## [2.7.0-0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.6.0...2.7.0-0) (2023-01-13)


### Features

* Migrer vers le SSR ([7a558b0](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7a558b0a204bf1c6754e3dd877b8e510644edfe3))
* Migrer vers le SSR ([d9e9f4f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d9e9f4fd09813ab6605109b44b187410c8a8de3a))


### Bug Fixes

* Protéger les routes external si l'origine est pas whitelistée ([b8156e6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b8156e60a73cf145f9f94e8451215e4d71414dea))

## [2.6.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.5.2...2.6.0) (2023-01-11)


### Features

* add statud editor page ([c569e6e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c569e6e1fa4c287fd519fc12168a62cc2ede6239))
* display status editor ([92578fd](https://gitlab.com/france-identite/rendez-vous-mairie/commit/92578fd5185c7c090a22e02a9ebeeb24c25e5b6a))
* Google Site Verification code ([895507f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/895507ff17e3ff2582740c67920cb47f1678bf75))
* Intégration du nouvel éditeur Numesia ([827e0af](https://gitlab.com/france-identite/rendez-vous-mairie/commit/827e0afcb1ca6a1a78d03f637c634a060352eaca))
* Migration de requests_async vers httpx ([74c48ed](https://gitlab.com/france-identite/rendez-vous-mairie/commit/74c48ed64d3385f8cca04d3b2c6402a19ab4390d))
* Rajouter SmartAgenda à la liste des éditeurs + correction de la gestion d'erreur availableTimeSlots ([41e8cdc](https://gitlab.com/france-identite/rendez-vous-mairie/commit/41e8cdc9ae9e25a283a43950a770a84c3cd4cf47))
* start unit test desktop mobile predemande search ([d19e767](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d19e767d850cbf61157a6657bb60249863141d52))
* test predemande search component ([a07c1ac](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a07c1ac726ec8fa08b66835f4f76c79634bf12d4))


### Bug Fixes

* bug server ([b2eda19](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b2eda196d12b8ea69aada1f1d3a0723c9a98f2b9))

### [2.5.2](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.5.1...2.5.2) (2023-01-03)


### Bug Fixes

* regex validation ([f1de52a](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f1de52afaa276569707e85042e4d38221e58d5fc))

### [2.5.1](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.5.0...2.5.1) (2022-12-27)

## [2.5.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.4.1...2.5.0) (2022-12-27)


### Features

* add version text to footer ([8e211f4](https://gitlab.com/france-identite/rendez-vous-mairie/commit/8e211f43155cce77f557b13997da791a67cbf1e6))
* delete mobile search component' ([7aee24b](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7aee24b24f45cb343b56e14b77022bd4da1aa4df))
* display version and date release in the footer + update standard version .versionrc.js ([d9fab7f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d9fab7f5cb835ea05279dc3ea60d0de5b5cd0aa9))
* display ville list ([db77be4](https://gitlab.com/france-identite/rendez-vous-mairie/commit/db77be4d7d868b67d0af7d5987d150e79714cfd1))
* Gérer les villes introuvables et quand le serveur ne répond pas ([d454588](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d45458807d5223ea1315a1243f620d59df976a25))
* update the template mobile search by city ([c2392cb](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c2392cbcd309fabeee1e5036b433f8ab03c89e06))
* updta name component desktop search to search ([4a53a92](https://gitlab.com/france-identite/rendez-vous-mairie/commit/4a53a92afae61f9e530c82758c91b69b14c3d6c2))


### Bug Fixes

* Ajouter le nom de l'éditeur dans les erreurs liées aux requêtes éditeurs... ([f7f5e4c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f7f5e4cfa75a0f86a8e4136f1ba4cdfacdeb90c8))
* Corriger le lien du header sur la page de recherche créneaux ([ebdd91c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ebdd91cb8040c81113898902789ccfc9d971160d))
* Discard environment.ts changes and code  optimization ([d6730e5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d6730e5c01ea8b1b76a21104dfa19a14b2bec27e))
* Gérer les erreurs 500 imprévus pour ne pas être camouflés par des erreurs CORS ([53769cb](https://gitlab.com/france-identite/rendez-vous-mairie/commit/53769cb3b4ec2628ec727667c63f16b41b114385))
* search by city params display ([158c401](https://gitlab.com/france-identite/rendez-vous-mairie/commit/158c401467ad80e755822163d0db5b571ada82fd))

### [2.4.1](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.4.0...2.4.1) (2022-12-20)


### Features

* add new proposition ([2dc9eb3](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2dc9eb38d6b75f07ce9affc97b480ff3b9396acb))
* add search page by city name version desktop and reslove url dynamique ([2505125](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2505125e53010ac50ca78fe32d6efb704c49cb88))


### Bug Fixes

* Forcer le mode desktop dans les tests du composant portal.appointment ([a34a043](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a34a043b14185271a79004b2115a5ba8bbb54d0e))
* Lancer l'application dans le container avec la commande uvicorn au lieu de lancer le script main.py ([e0a4c0f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e0a4c0fa2759ee96cbcd5c19916ab97690a0d617))
* ne pas essayer de close les websockets ([774faf9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/774faf95a714ee53cbd5cc87ffd4a899208b1754))
* Supprimer le code postal et le nom de la ville dans l'adresse publique des mairies ([b3009d0](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b3009d005fd0764a3bfca7b55eacc10f22d2b851))
* valider les données envoyées par websocket avant la recherche ([94276b6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/94276b6240a4e62f1d771f74f2d191051ccbec0b))

## [2.4.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.3.0...2.4.0) (2022-12-15)


### Features

* Nouvel endpoint pour avec les informations d'une ville par nom ([dcd7702](https://gitlab.com/france-identite/rendez-vous-mairie/commit/dcd7702a6bebc87061a9f7e1a762ec6610b3936d))


### Bug Fixes

* Les variables d'env des annonces peuvent être vides ([aa4b017](https://gitlab.com/france-identite/rendez-vous-mairie/commit/aa4b017e49f947143edafdd4b3efcec0665c6656))

## [2.3.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/v2.2.0...v2.3.0) (2022-12-10)


### Features

* add list tag ([0afb3f5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/0afb3f5fae8addc201387b7ae559712fec1b18be))
* add message blue for motif retrait ([e25b577](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e25b577499c42ff213ee2acb0414cbeb95172d32))
* Ajouter plus de détails dans les logs des erreurs éditeurs ([1bc3f5e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1bc3f5eb918a26568e27a61c5fb71bd8e1c1b646))
* Bumper automatiquement la version dans le fichier pyproject.toml quand... ([eea6097](https://gitlab.com/france-identite/rendez-vous-mairie/commit/eea60977c1e8a15b3d490b5d6568647721dd52eb))
* bumping version python angular ([bbb7007](https://gitlab.com/france-identite/rendez-vous-mairie/commit/bbb70071b8397f3b62397432336a21c7c335673f))
* Nouveau endpoint pour les annonces globales, configurables depuis les... ([63bd910](https://gitlab.com/france-identite/rendez-vous-mairie/commit/63bd910749e5621e1ee7378bb430d157526abca4))
* Nouvel endpoint pour les annonces globales, configurables depuis les... ([2484e1b](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2484e1bc3880d252e963717200fc19d0c22f223b))


### Bug Fixes

* add aria-busy to the container of the spinner ([e9d3d7a](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e9d3d7a1895893c1fa5aa61cde210c62523ebecb))
* Dans un fieldset doit avoir un legend + supprimer le champ caché... ([696012e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/696012e851340689e17c9f57c7284ac038d81f91))
* Enlever la restriction sur les innerHTML ([03c9772](https://gitlab.com/france-identite/rendez-vous-mairie/commit/03c97720f9505d2dea538c3a6652d302d53e7a86))
* Enlever la restriction sur les innerHTML ([d7a132f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d7a132f78069b02964bb2e11f2bd145289875657))
* la CSP n'autorisait pas les strings dans innerHTML ([b83ab7b](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b83ab7bb5a1eec077cb6fb862c12b655797ec951))
* label boutton de navigation ([38b155c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/38b155c390ea99bb0d2138cbde0d2aee044995af))
* messahe and the style mobile ([be84ba5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/be84ba53c5a59d9ab21ca675b663ed2a97019256))
* quelque bug d'accessibilité ([3ab554c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/3ab554c673c1fa854623f8e4e97f9cfd505f6954))
* remove duplicate id from select balise ([e1ce2c6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e1ce2c68948e8fe80841e1940ef239412090e1e0))
* scroll to top when cliock en savoir plus boutton ([ccd71b9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ccd71b9e5db1837b17d48dd25b1b3705177c6879))
* update aria label value of dektop slots component ([b40d998](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b40d99823e8245558cc9d8ca1b591eb02f169821))

## [2.2.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/v2.2.0-0...v2.2.0) (2022-11-30)

## [2.2.0-0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/v2.1.0...v2.2.0-0) (2022-11-30)


### Features

* add autoflake8 + migrate to pipenv + configure pipefile +update ci cd with pipenva ([7fce68e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7fce68e3cd557cabd929f61b9a349704ea9b80b5))
* bump version ([ce1315b](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ce1315b9441f91708caf35efaa8d8874ef535879))
* migrate to pipenv ([09cfe17](https://gitlab.com/france-identite/rendez-vous-mairie/commit/09cfe17a8e234bd1def017d62f27c2434d1f5d7f))


### Bug Fixes

* (SEO) Avoir un index.html pour chaque environnement pour contrôler... ([7e21e31](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7e21e318ab6ada8b61052df05d6895923d7260b4))
* (SEO) Avoir un index.html pour chaque environnement pour contrôler... ([4c2ca1c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/4c2ca1c86b5a6f478298367f8f01539ec8427094))
* Ajouter des codes d’erreur HTTP pour des routes API ([830eb84](https://gitlab.com/france-identite/rendez-vous-mairie/commit/830eb8480b8025849a18572349707f47a1303285))
* Ajouter les headers de sécurité au backend ([e7a0007](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e7a000747e37b265d2b4725319a48c614a7c8139))
* Corrgier le format de log backend pour avoir le bon timestamp et log.level ([d05e22e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d05e22e2d9f64527fdc26e8c31aebbd2b76b0cee))
* Les requêtes qui viennent de notre frontend (nom de domaine whitelisté) n'ont plus besoin de token de sécurité ([4d6a798](https://gitlab.com/france-identite/rendez-vous-mairie/commit/4d6a79830ab0bb27c6ac3b23c598c6f7cd6b2016))
* pipefile script ([dcb3ba2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/dcb3ba23906bb72d882a297d7ac3141943c5bf12))
* search_close_meeting_points doit retourner une copie des meeting_points... ([f2d548f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f2d548f64c743ce1288cb1b5e96df95ed73b78a8))
* Supprimer les champs privés _editor_name et _internal_id sans affecter les meeting_points ([9bdd185](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9bdd18586dccc57fa74a0acec6dcbbf54dea1b0e))
* Valider la distance max de recherche + cacher _internal_id et _editor_name dans le websocket ([a27d7db](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a27d7db4e15b389523db0861b9a7d66d7cfa08bf))
* Vérifier la connexion websocket toutes les 5 secondes ([7c8be48](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7c8be4877b75ed5d5c4130141325dcff7022cc9f))

## [2.2.0-0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/v2.1.0...v2.2.0-0) (2022-11-30)


### Features

* add autoflake8 + migrate to pipenv + configure pipefile +update ci cd with pipenva ([7fce68e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7fce68e3cd557cabd929f61b9a349704ea9b80b5))
* bump version ([ce1315b](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ce1315b9441f91708caf35efaa8d8874ef535879))
* migrate to pipenv ([09cfe17](https://gitlab.com/france-identite/rendez-vous-mairie/commit/09cfe17a8e234bd1def017d62f27c2434d1f5d7f))


### Bug Fixes

* (SEO) Avoir un index.html pour chaque environnement pour contrôler... ([7e21e31](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7e21e318ab6ada8b61052df05d6895923d7260b4))
* (SEO) Avoir un index.html pour chaque environnement pour contrôler... ([4c2ca1c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/4c2ca1c86b5a6f478298367f8f01539ec8427094))
* Ajouter des codes d’erreur HTTP pour des routes API ([830eb84](https://gitlab.com/france-identite/rendez-vous-mairie/commit/830eb8480b8025849a18572349707f47a1303285))
* Ajouter les headers de sécurité au backend ([e7a0007](https://gitlab.com/france-identite/rendez-vous-mairie/commit/e7a000747e37b265d2b4725319a48c614a7c8139))
* Corrgier le format de log backend pour avoir le bon timestamp et log.level ([d05e22e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d05e22e2d9f64527fdc26e8c31aebbd2b76b0cee))
* Les requêtes qui viennent de notre frontend (nom de domaine whitelisté) n'ont plus besoin de token de sécurité ([4d6a798](https://gitlab.com/france-identite/rendez-vous-mairie/commit/4d6a79830ab0bb27c6ac3b23c598c6f7cd6b2016))
* pipefile script ([dcb3ba2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/dcb3ba23906bb72d882a297d7ac3141943c5bf12))
* search_close_meeting_points doit retourner une copie des meeting_points... ([f2d548f](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f2d548f64c743ce1288cb1b5e96df95ed73b78a8))
* Supprimer les champs privés _editor_name et _internal_id sans affecter les meeting_points ([9bdd185](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9bdd18586dccc57fa74a0acec6dcbbf54dea1b0e))
* Valider la distance max de recherche + cacher _internal_id et _editor_name dans le websocket ([a27d7db](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a27d7db4e15b389523db0861b9a7d66d7cfa08bf))
* Vérifier la connexion websocket toutes les 5 secondes ([7c8be48](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7c8be4877b75ed5d5c4130141325dcff7022cc9f))

## [2.1.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/v2.0.0...v2.1.0) (2022-11-21)


### Features

* add predemande unit test and fix the marge predemande mobile ([90a1c67](https://gitlab.com/france-identite/rendez-vous-mairie/commit/90a1c67a1a4c5992bd77935b53ae61fa01f30672))
* Ajouter l'éditeur Solocal ([5f846a2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/5f846a2926afc1405186d54d0190c60e6b4e4c21))
* handle geolocalisation un defined with aa modal ([240c165](https://gitlab.com/france-identite/rendez-vous-mairie/commit/240c1651833025075b6e89f8fd658ef4eda49b0a))
* update version display ([de5b0a8](https://gitlab.com/france-identite/rendez-vous-mairie/commit/de5b0a87438e35b9f457b2975bcc31c99cd9f1c2))


### Bug Fixes

* Image de secours quand le lien logo est 404 ([65105a6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/65105a6c6d38252427b31c5b688a07c4dbde8f7d))
* Passer les variables reason et documents_number dans le websocket ([c1c28e2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c1c28e2d00d59bbf3358336a540bce1056b9850b))
* predemande id validation ([d4a0153](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d4a0153c197f7a8334efc1755ac03fbb33ad0ca9))
* Robots.txt et Sitemap.xml dynamiques pour chaque environnement ([1892d3c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/1892d3c8968e1ce0a32138c5541a2ea78cfcf3b6))
* Supprimer la timezone de la datetime envoyée par les éditeurs ([efb1402](https://gitlab.com/france-identite/rendez-vous-mairie/commit/efb1402a9ad2bbbf11c62424d710342ab92ef488))

## [2.0.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/v1.2.0...v2.0.0) (2022-11-15)


### Features

* add motif and number ([a320294](https://gitlab.com/france-identite/rendez-vous-mairie/commit/a320294a85cebfeb920f110b7a7c47fe4c6ae298))
* add motif document number from slots position ([9f00586](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9f0058683aa8aa509a50800e1a573770971011ba))
* add regex to application id ([5301c04](https://gitlab.com/france-identite/rendez-vous-mairie/commit/5301c04a86335bca66b5e0c396a95d33d4fe4b42))
* add unit test predemande list desktop ([eac05fd](https://gitlab.com/france-identite/rendez-vous-mairie/commit/eac05fdd7d7eee624e71b4328952f1d2e35e862c))
* Ajouter le nombre d'éditeurs qui ont eu des erreurs pendant la recherche par websocket ([433e5aa](https://gitlab.com/france-identite/rendez-vous-mairie/commit/433e5aafa2a6b9ed4b15f4c522b3ddc69d180212))
* display version automatically ([26772a5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/26772a5d1576352839e39e7325a1409d407429c9))
* Limiter le nombre de requêtes HTTP par personne et le nombre de message par websocket ([9adeaf0](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9adeaf083f77c47074cc3553d2f49c973a85e2ac))
* read app version and display it automatically ([ff46817](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ff468172144230b716981a22ae8962f81002e262))

## [1.2.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/v1.1.0...v1.2.0) (2022-11-02)


### Features

* Ajouter Ypok aux éditeurs ([554da23](https://gitlab.com/france-identite/rendez-vous-mairie/commit/554da23db4552e8d797b3086d2a60d6eb1d743e0))


### Bug Fixes

* desactivate search button until there is result ([c8fad2c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c8fad2c2425e455a651753c419ef6f7ebd5e0b8c))
* spinner hide before end of search ([be92317](https://gitlab.com/france-identite/rendez-vous-mairie/commit/be92317bfd19c900116f50680746bb654c3f8eef))

## 1.1.0 (2022-10-27)


### Features

* add contacte nous to footer ([2c2f749](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2c2f749c360626f66d10c002f403ad202d27488f))
* clean code ([616970c](https://gitlab.com/france-identite/rendez-vous-mairie/commit/616970c7f3d9e8bd2702e7ee75fd5ce6276985e7))
* clean code for desktop search and desktop search and portal appointmenet componenets ([9c0080b](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9c0080b5e2ba9d2658844822800276497ffc5084))
* create tu for backend ([3473892](https://gitlab.com/france-identite/rendez-vous-mairie/commit/34738929e27fcae0ddf7d27f66d76e6af7810a23))
* develop display sloyts mobile ([ddbc858](https://gitlab.com/france-identite/rendez-vous-mairie/commit/ddbc858df9ac0e33d63296ee6e9e4235df755df7))
* develop integration test for internal route ([74c5e42](https://gitlab.com/france-identite/rendez-vous-mairie/commit/74c5e42bd0f7e80b6421d282eec12fd7cc5e7fc8))
* develop mobile version ([79cbc0d](https://gitlab.com/france-identite/rendez-vous-mairie/commit/79cbc0d4cacdfa7102e37504665ea4c26b46a84c))
* develop modal desktop mobile ([f56ffd2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/f56ffd2ba03fbc0391ea2f82d2fc2633bf49a5d9))
* display list of slots prposition ([b736dc7](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b736dc7efcb33af57812276562dc21269ebe05e4))
* dynamique connextion to backend depending on type of environment ([9cbce04](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9cbce04f0c4c58339fde2249a14fa22e7232a8ed))
* initiate-squlette-unittest-backend ([6fa9292](https://gitlab.com/france-identite/rendez-vous-mairie/commit/6fa92927cd603ddaf4cec7cc0faa0bfa92b3438b))
* make footer position responsive ([31de7f8](https://gitlab.com/france-identite/rendez-vous-mairie/commit/31de7f844b9f2a394b1902a7e708209573bfba33))
* responsive desktop mobile predemande ([57ea541](https://gitlab.com/france-identite/rendez-vous-mairie/commit/57ea54182342ed399b1a509b3ab65916f0f73758))
* restructrure and routing ([9787acd](https://gitlab.com/france-identite/rendez-vous-mairie/commit/9787acde4ad0987aad13170f7024591df6f4a63d))
* seperate gouv service and optimize search componenet ([2d46fb6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/2d46fb6406940ee6ac6446130cbb2d9cb7aac889))
* start refactoring search_meeting_points methode ([165d399](https://gitlab.com/france-identite/rendez-vous-mairie/commit/165d39963de50f5e6e77137dface13de7c201017))
* update messages slots ([68e300e](https://gitlab.com/france-identite/rendez-vous-mairie/commit/68e300e3c8dc464f58dfa5aafac43742632bf8f4))
* update modal locolisation ([b7019f9](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b7019f932709479a3f0d2203e8e13387a0d450d3))
* update runner name ([8ddc559](https://gitlab.com/france-identite/rendez-vous-mairie/commit/8ddc559eb75699023ef3f191f18de0103403c606))


### Bug Fixes

* active button recheche when address is displayed ([c6528a2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/c6528a29ec848d93ed034047731f67b30ea43259))
* ajustement de la distance message desktop and mobile ([60a3002](https://gitlab.com/france-identite/rendez-vous-mairie/commit/60a3002b272d6c073c857f38984203b578509c25))
* alert heading ([bb9e939](https://gitlab.com/france-identite/rendez-vous-mairie/commit/bb9e9391c61a4267bfab49f52e9abd87e2eb9575))
* bug minute hour and the display ([bd6c0f7](https://gitlab.com/france-identite/rendez-vous-mairie/commit/bd6c0f7b8dfa57a2a13b9b705825f20fe1146141))
* bug perte de connexion durant la recherche ([bc250b6](https://gitlab.com/france-identite/rendez-vous-mairie/commit/bc250b6ae04f748eb5e01d27aa8374f67a133027))
* bug position geolocalisation denied ([85d8cc2](https://gitlab.com/france-identite/rendez-vous-mairie/commit/85d8cc2317e3a92f4282d9893c897dc938b95291))
* ci-cd ([71e95bb](https://gitlab.com/france-identite/rendez-vous-mairie/commit/71e95bba50261cf97456b23af1b6b1cf1a1bdaef))
* disable button when user click sur * par le souris  et lorsque il... ([d15fa49](https://gitlab.com/france-identite/rendez-vous-mairie/commit/d15fa4917ffe05f9fabca37e264cb13397f1bfe3))
* fix retour ([25b9f65](https://gitlab.com/france-identite/rendez-vous-mairie/commit/25b9f65d045e4c19cad9e54b2f2b4ac652634613))
* le style et le message ([00d8eaa](https://gitlab.com/france-identite/rendez-vous-mairie/commit/00d8eaa4a540fb2844cc89248edfe8e531458a6e))
* radio default value and optimize something ([218c5d3](https://gitlab.com/france-identite/rendez-vous-mairie/commit/218c5d3fa182cf14858c7a94c8347e26a55b862f))
* search-component ([7e4871a](https://gitlab.com/france-identite/rendez-vous-mairie/commit/7e4871ace889e157ae96e418ac07b9a751105ed6))
