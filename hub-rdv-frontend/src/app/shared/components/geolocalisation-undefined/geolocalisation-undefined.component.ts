import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ModalService } from '@shared/services/modal.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'rdv-geolocalisation-undefined',
  templateUrl: './geolocalisation-undefined.component.html',
  styleUrls: ['./geolocalisation-undefined.component.scss'],
})
export class GeolocalisationUndefinedComponent implements OnInit, OnDestroy {
  @ViewChild('showGeoUndefinedButton') showGeoUndefinedButton?: ElementRef;
  subscription$?: Subscription;
  constructor(private loadingService: ModalService) {}

  ngOnInit(): void {
    this.subscription$ = this.loadingService.showGeoUndefined.subscribe((state) => {
      if (state.showGeoUndefined && this.showGeoUndefinedButton) {
        this.showGeoUndefinedButton.nativeElement.click();
      }
    });
  }
  ngOnDestroy(): void {
    this.subscription$?.unsubscribe();
  }
}
