import 'zone.js/dist/zone-node';

import {APP_BASE_HREF} from '@angular/common';
import {ngExpressEngine} from '@nguniversal/express-engine';
import express from 'express';
import {existsSync} from 'fs';
import {join} from 'path';
import onFinished from 'on-finished';
import * as winston from 'winston';
import * as path from 'path';

import {AppServerModule} from './src/main.server';
const ecsFormat = require('@elastic/ecs-winston-format');


const logger = winston.createLogger({
  level: 'info',
  format: ecsFormat(),
  transports: [
    new winston.transports.Console()
  ]
})

// The Express app is exported so that it can be used by serverless Functions.
export function app(): express.Express {
  const server = express();
  const distFolder = join(process.cwd(), 'dist/hub-rdv-frontend/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/main/modules/express-engine)
  server.engine('html', ngExpressEngine({
    bootstrap: AppServerModule,
    inlineCriticalCss: false
  }));

  server.set('view engine', 'html');
  server.set('views', distFolder);

  server.use(
    function applyTiming( request, response, next ) {
      var startedAt = Date.now();

      onFinished(
        response,
        function logRequestDuration( error ) {
          var requestDuration = ( Date.now() - startedAt );
          logger.info('Access', {
              "method": request.method,
              "path": request.url,
              "responseTime": requestDuration,
              "realip": request.headers['x-real-ip'] || request.ip,
              "referrer": request.get('Referrer'),
              "source_side": "Frontend"
          })
      });
      next();
    });

  server.use(function(req, res, next) {
    res.setHeader("X-Frame-Options", "DENY");
    res.setHeader("Content-Security-Policy", "script-src 'self' 'unsafe-inline' 'unsafe-eval' https://www.google.com https://gstatic.com https://www.gstatic.com; object-src 'self'");
    res.setHeader("X-Content-Type-Options", "nosniff");
    res.setHeader("X-XSS-Protection", "1; mode=block");
    next();
  });
    
  server.get('/robots.txt', function(req, res) {
      if (process.env['ANGULAR_ENVIRONMENT'] == 'production'){
          res.sendFile(path.resolve(distFolder + '/robots.txt'));
      } else {
          res.sendFile(path.resolve(distFolder + '/robots-dev.txt'));
      }
  })

  // Example Express Rest API endpoints
  // server.get('/api/**', (req, res) => { });
  // Serve static files from /browser
  server.get('*.*', express.static(distFolder, {
    maxAge: '1y'
  }));

  // All regular routes use the Universal engine
  server.get('*', (req, res) => {
    res.render(indexHtml, { req, providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }] });
  });

  return server;
}

function run(): void {
  const port = process.env['PORT'] || 4000;

  // Start up the Node server
  const server = app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
