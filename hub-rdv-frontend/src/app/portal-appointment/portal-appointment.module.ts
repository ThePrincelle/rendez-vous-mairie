import { CommonModule, NgOptimizedImage, registerLocaleData } from '@angular/common';
import { PortalAppointmentComponent } from './portal-appointment/portal-appointment.component';
import { AppointmentListComponent } from './appointment-list/appointment-list.component';
import { SharedModule } from '@shared/shared.module';
import { PortalAppointmentRoutingModule } from './portal-appointment-routing.module';
import localeFr from '@angular/common/locales/fr';
import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { RecaptchaModule, RecaptchaFormsModule, RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module, ReCaptchaV3Service } from 'ng-recaptcha';
import { SearchComponent } from './search/search.component';
import { environment } from 'src/environments/environment';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SearchByCityComponent } from './search-by-city/search-by-city.component';
import { CitiesComponent } from './cities/cities.component';
import { DepartmentsComponent } from './departments/departments.component';
import { SearchByDepartmentComponent } from './search-by-department/search-by-department.component';
import { DepartmentCitiesComponent } from './department-cities/department-cities.component';
import { MessageComponent } from './message/message.component';
import { SlotsComponent } from './slots/slots.component';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    PortalAppointmentComponent,
    AppointmentListComponent,
    SearchComponent,
    SearchByCityComponent,
    CitiesComponent,
    DepartmentsComponent,
    SearchByDepartmentComponent,
    DepartmentCitiesComponent,
    SlotsComponent,
    MessageComponent,
  ],
  imports: [
    NgxSpinnerModule,
    CommonModule,
    SharedModule,
    PortalAppointmentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaFormsModule,
    RecaptchaModule,
    RecaptchaV3Module,
    AutocompleteLibModule,
    NgOptimizedImage,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    ReCaptchaV3Service,
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useValue: environment.recaptcha.site_key,
    },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PortalAppointmentModule {}
