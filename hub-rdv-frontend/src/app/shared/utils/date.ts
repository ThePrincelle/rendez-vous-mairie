import moment from 'moment';
export abstract class DateUtils {
  static getDateFormat(date: string | undefined): string {
    moment.locale('fr');
    return moment(date).format('dddd DD MMMM YYYY');
  }

  static getDate(date: string | undefined): string {
    return moment(date).format('YYYY-MM-DD');
  }
  static getDefaultStartDate(): string {
    const startMoment: moment.Moment = moment(new Date());
    return startMoment.format('YYYY-MM-DD');
  }

  static getDefaultEndDate(date: any | undefined): string {
    if (!date) {
      const startMoment: moment.Moment = moment(new Date()).add(3, 'M');
      return startMoment.format('YYYY-MM-DD');
    } else {
      const startMoment: moment.Moment = moment(new Date(date)).add(3, 'M');
      return startMoment.format('YYYY-MM-DD');
    }
  }

  static getEndDateMax(date: any | undefined): string {
    const startMoment: moment.Moment = moment(new Date(date)).add(3, 'M');
    return startMoment.format('YYYY-MM-DD');
  }
}
