import { NgModule, SecurityContext } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { CoreRoutingModule } from './core-routing.module';
import { MarkdownModule } from 'ngx-markdown';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CguMentionLegalComponent } from './cgu-mention-legal/cgu-mention-legal.component';
import { SavoirPlusComponent } from './savoir-plus/savoir-plus.component';
import { AccessibilityComponent } from './accessibility/accessibility.component';
import { StatusComponent } from './status/status.component';
import { DonneesPersonnellesComponent } from './donnees-personnelles/donnees-personnelles.component';
import { DeterritorialisationComponent } from './deterritorialisation/deterritorialisation.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [
    CguMentionLegalComponent,
    SavoirPlusComponent,
    AccessibilityComponent,
    StatusComponent,
    DonneesPersonnellesComponent,
    DeterritorialisationComponent,
  ],
  imports: [
    CoreRoutingModule,
    CommonModule,
    HttpClientModule,
    MarkdownModule.forRoot({ loader: HttpClient, sanitize: SecurityContext.NONE }),
    NgOptimizedImage,
    SharedModule,
  ],
})
export class CoreModule {}
