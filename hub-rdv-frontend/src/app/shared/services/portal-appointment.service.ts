import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environment/*';
import { SearchCriteria } from '@shared/classes/search-criteria';
import { catchError, Observable, of, throwError } from 'rxjs';
import { Municipality } from '../classes/municipality';
@Injectable({
  providedIn: 'root',
})
export class PortalAppointmentService {
  constructor(private http: HttpClient) {}

  getMunicipality(cityName: string): Observable<any> {
    const url: string = environment.api_url + '/searchCity';
    const headers = new HttpHeaders().set('content-type', 'application/json');
    const params = new HttpParams().set('name', cityName);
    return this.http.get(url, { headers: headers, params: params });
  }

  getMunicipalities(): Observable<any> {
    const url: string = environment.api_url + '/getManagedMeetingPoints';
    const headers = new HttpHeaders().set('content-type', 'application/json');
    return this.http.get(url, { headers: headers }).pipe(catchError(this.handleError));
  }
  private handleError(error: Response | any) {
    //Your other codes

    if (error.status == 0) {
      //or whatever condition you like to put
      return throwError(() => new Error('Something bad happened; please try again later.'));
    }
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }
  getMunicipalitiesOffline(searchCriteria: SearchCriteria): Observable<any> {
    const url: string = environment.api_url + '/searchOfflineMeetingPoints';
    const headers = new HttpHeaders().set('content-type', 'application/json');
    let params = new HttpParams();

    if (searchCriteria.longitude && searchCriteria.latitude && searchCriteria.radius_km) {
      params = params.set('longitude', searchCriteria.longitude);
      params = params.set('latitude', searchCriteria.latitude);
      params = params.set('radius_km', searchCriteria.radius_km);
    }
    const paramsArray = params.keys().map((x) => ({ [x]: params.get(x) }));
    return this.http.get(url, { headers: headers, params: params });
  }
}
