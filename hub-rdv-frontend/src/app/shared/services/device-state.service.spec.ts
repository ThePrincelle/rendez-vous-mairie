import { TestBed } from '@angular/core/testing';

import { DeviceStateService } from './device-state.service';

describe('DeviceStateService', () => {
  let service: DeviceStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeviceStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return false if isMobileResolution called and window innerwidth > 768', () => {
    window.innerWidth = 769;
    const result = service.isMobileResolution();
    expect(result).toBeFalsy();
  });

  it('should return true if isMobileResolution called and window innerwidth < 768', () => {
    window.innerWidth = 767;
    const result = service.isMobileResolution();
    expect(result).toBeTruthy();
  });

  it('should return correct screen code based on window innerwidth when getScreen is called', () => {
    window.innerWidth = 575;
    expect(service.getScreen()).toBe('XS');
    window.innerWidth = 767;
    expect(service.getScreen()).toBe('SM');
    window.innerWidth = 991;
    expect(service.getScreen()).toBe('MD');
    window.innerWidth = 1247;
    expect(service.getScreen()).toBe('LG');
    window.innerWidth = 1249;
    expect(service.getScreen()).toBe('XL');
  });
});
