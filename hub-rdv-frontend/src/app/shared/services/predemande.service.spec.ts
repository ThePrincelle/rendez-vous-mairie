import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { PredemandeService } from './predemande.service';

describe('PredemandeService', () => {
  let service: PredemandeService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpClient],
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(PredemandeService);
  });
  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http.get if service get is called', () => {
    const httpGetSpy = spyOn(httpClient, 'get');
    service.get('', ['1234567890', '1111111111']);
    expect(httpGetSpy).toHaveBeenCalled();
  });
});
