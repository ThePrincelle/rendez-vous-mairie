/*import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ReCaptchaV3Service } from 'ng-recaptcha';

import { PredemandeComponent } from './predemande.component';
import { fakeRecaptchaV3Service } from '@shared/mocks/mock.recaptcha.service';
import { By } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('PredemandeComponent', () => {
  let component: PredemandeComponent;
  let fixture: ComponentFixture<PredemandeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      declarations: [PredemandeComponent],
      imports: [HttpClientTestingModule],
      providers: [
        FormBuilder,
        HttpClient,
        {
          provide: ReCaptchaV3Service,
          useValue: fakeRecaptchaV3Service,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PredemandeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change validatorError to false if input is valid', () => {
    component.form.get('predemandes')?.setValue('123456abcd');
    expect(component.ValidateError).toBeFalsy();

    component.form.get('predemandes')?.setValue('123456abcd 77777PPPPP');
    expect(component.ValidateError).toBeFalsy();
  });

  it('should change validatorError to true if input is not valid', () => {
    component.form.get('predemandes')?.setValue('1236abcd');
    expect(component.ValidateError).toBeTruthy();

    component.form.get('predemandes')?.setValue('123456abcd 77777PPP!!');
    expect(component.ValidateError).toBeTruthy();
  });

  it('should not launch search if onSearch called on invalid form', () => {
    let recaptchaServiceSpy = spyOn(fakeRecaptchaV3Service, 'execute');
    component.form.get('predemandes')?.setValue('1236abcd');
    component.onSearch();
    expect(recaptchaServiceSpy).not.toHaveBeenCalled();
  });

  it('renders Predemande List Component if there are results', () => {
    component.predemande_results = new Array();
    component.predemande_results.push({
      meeting_point: 'Mairie ANNEXE LILLE-SECLIN',
      datetime: '2022-12-19T10:00Z',
      management_url: 'http://www.ville-seclin.fr/rendez-vous/predemande?num=6123155111',
      cancel_url: 'http://www.ville-seclin.fr/rendez-vous/annulation?num=6123155111',
    });
    component.desktop = true;
    fixture.detectChanges();
    const desktopSearchElement = fixture.debugElement.query(By.css('rdv-desktop-predemande-list'));
    expect(desktopSearchElement).toBeTruthy();

    component.desktop = false;
    fixture.detectChanges();
    const mobileSearchElement = fixture.debugElement.query(By.css('rdv-mobile-predemande-list'));
    expect(mobileSearchElement).toBeTruthy();
  });
});
*/
