"""This script helps to automate the lint and  the format code of the backend
"""
from invoke import task


@task
def format(c):
    """Automate the execute of the format cmds that change the code format

    Args:
        c (_type_): convention is to use c, ctx or context
    """
    c.run("python -m pipenv run format-black", env={"PIPENV_DONT_LOAD_ENV": "1"})
    c.run("python -m pipenv run format-autoflake", env={"PIPENV_DONT_LOAD_ENV": "1"})
    c.run("python -m pipenv run format-isort", env={"PIPENV_DONT_LOAD_ENV": "1"})


@task
def lint(c):
    """Automate the execute of the lint cmds that verify the code format

    Args:
        c (_type_): convention is to use c, ctx or context
    """
    c.run("python -m pipenv run lint-flake8", env={"PIPENV_DONT_LOAD_ENV": "1"})
    c.run("python -m pipenv run lint-black", env={"PIPENV_DONT_LOAD_ENV": "1"})
    c.run("python -m pipenv run lint-isort", env={"PIPENV_DONT_LOAD_ENV": "1"})
