const axios = require('axios')
require('dotenv').config()
const fs = require('fs');


async function getAllCities() {
    const config = {
        method: 'get',
        url: 'http://localhost:8081/api/getManagedMeetingPoints',
        headers: { 'x-hub-rdv-auth-token': 'test-token' }
    }
    try {
        let res = await axios(config)
        return res.data
    } catch (error) {
        console.error(error)
        return []
    }
}

function getAllDepartments() {
    return ['corse', 'ain', 'aisne', 'allier', 'alpes-de-haute-provence', 'hautes-alpes', 'alpes-maritimes', 'ardeche', 'ardennes', 'ariege', 'aube', 'aude', 'aveyron', 'bouches-du-rhone', 'calvados', 'cantal', 'charente', 'charente-maritime', 'cher', 'correze', 'cote-d-or', 'cotes-d-armor', 'creuse', 'dordogne', 'doubs', 'drome', 'eure', 'eure-et-loir', 'finistere', 'gard', 'haute-garonne', 'gers', 'gironde', 'herault', 'ille-et-vilaine', 'indre', 'indre-et-loire', 'isere', 'jura', 'landes', 'loir-et-cher', 'loire', 'haute-loire', 'loire-atlantique', 'loiret', 'lot', 'lot-et-garonne', 'lozere', 'maine-et-loire', 'manche', 'marne', 'haute-marne', 'mayenne', 'meurthe-et-moselle', 'meuse', 'morbihan', 'moselle', 'nievre', 'nord', 'oise', 'orne', 'pas-de-calais', 'puy-de-dome', 'pyrenees-atlantiques', 'hautes-pyrenees', 'pyrenees-orientales', 'bas-rhin', 'haut-rhin', 'rhone', 'haute-saone', 'saone-et-loire', 'sarthe', 'savoie', 'haute-savoie', 'paris', 'seine-maritime', 'seine-et-marne', 'yvelines', 'deux-sevres', 
'somme', 'tarn', 'tarn-et-garonne', 'var', 'vaucluse', 'vendee', 'vienne', 'haute-vienne', 'vosges', 'yonne', 'territoire-de-belfort', 'essonne', 'hauts-de-seine', 'seine-saint-denis', 'val-de-marne', 'val-d-oise', 'guadeloupe', 'martinique', 'guyane', 'reunion', 'mayotte', 'saint-barthelemy', 'saint-martin', 'polynesie-francaise', 'nouvelle-caledonie']
}

async function buildSitemap() {
    let cities = await getAllCities();
    let departments = getAllDepartments()

    let sitemapText = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <!-- created with Free Online Sitemap Generator www.xml-sitemaps.com -->


    <url>
        <loc>https://rendezvouspasseport.ants.gouv.fr/</loc>
    </url>
    <url>
        <loc>https://rendezvouspasseport.ants.gouv.fr/villes</loc>
    </url>
    <url>
        <loc>https://rendezvouspasseport.ants.gouv.fr/departments</loc>
    </url>

`

    cities.forEach((city) => {
        sitemapText += `
        <url>
            <loc>https://rendezvouspasseport.ants.gouv.fr/ville/${city.decoded_city_name}</loc>
        </url>`
    })

    departments.forEach((department) => {
        sitemapText += `
    <url>
        <loc>https://rendezvouspasseport.ants.gouv.fr/departement/${department}</loc>
    </url>`
    })

    sitemapText +=  `</urlset>`

    fs.writeFile('src/sitemap.xml', sitemapText, (err) => {
        if (err) {
          console.error(err);
        }
      });
}

buildSitemap()