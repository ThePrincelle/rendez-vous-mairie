import { Location } from '@angular/common';
import { fakeAsync, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AccessibilityComponent } from './accessibility/accessibility.component';
import { CguMentionLegalComponent } from './cgu-mention-legal/cgu-mention-legal.component';
import { routes } from './core-routing.module';
import { SavoirPlusComponent } from './savoir-plus/savoir-plus.component';
import { StatusComponent } from './status/status.component';

describe('Routing Core : ', () => {
  let location: Location;
  let router: Router;
  let fixture;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [CguMentionLegalComponent, StatusComponent, SavoirPlusComponent, AccessibilityComponent],
    });

    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    router.initialNavigation();
  });

  it('navigate to "StatusComponent" takes you to /status', fakeAsync(() => {
    router.navigate(['/status']).then(() => {
      expect(location.path()).toBe('/status');
      expect();
    });
  }));

  it('navigate to "CGU-mentions-legales" takes you to /CGU-mentions-legales', fakeAsync(() => {
    router.navigate(['/CGU-mentions-legales']).then(() => {
      expect(location.path()).toBe('/CGU-mentions-legales');
      expect();
    });
  }));

  it('navigate to "SavoirPlusComponent" takes you to /savoir-plus', fakeAsync(() => {
    router.navigate(['/savoir-plus']).then(() => {
      expect(location.path()).toBe('/savoir-plus');
      expect();
    });
  }));
});
