import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StringFormatterService {
  constructor() {}

  format(str: string, ...replacements: string[]): string {
    return str.replace(/{(\d+)}/g, function (match, number) {
      return typeof replacements[number] != 'undefined' ? replacements[number] : match;
    });
  }
}
