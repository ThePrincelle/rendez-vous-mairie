import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-accessibility',
  templateUrl: './accessibility.component.html',
  styleUrls: ['./accessibility.component.scss'],
})
export class AccessibilityComponent implements OnInit {
  breadcrumbs?: Array<BreadcrumbItem>;
  constructor(private titleService: Title) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Accéssibilité',
        url: '/accessibilite',
      },
    ];
    this.titleService.setTitle('Accessibilité');
  }
}
