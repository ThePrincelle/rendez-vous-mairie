import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ModalService } from '@shared/services/modal.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'rdv-modal-user-denied',
  templateUrl: './modal-user-denied.component.html',
  styleUrls: ['./modal-user-denied.component.scss'],
})
export class ModalUserDeniedComponent implements OnInit, OnDestroy {
  @ViewChild('showModalButton') showModalButton?: ElementRef;
  subscription$?: Subscription;
  constructor(private loadingService: ModalService) {}

  ngOnInit(): void {
    this.subscription$ = this.loadingService.showLoading.subscribe((state) => {
      if (state.loading && this.showModalButton) {
        this.showModalButton.nativeElement.click();
      }
    });
  }
  ngOnDestroy(): void {
    this.subscription$?.unsubscribe();
  }
}
