import { Component, OnInit, ViewChild } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Municipality } from '@shared/classes/municipality';
import { PortalAppointmentService } from '@shared/services/portal-appointment.service';
import { Cities } from '@shared/utils/cities';
import { NgxSpinnerService } from 'ngx-spinner';
import { Utils } from '@shared/utils/utils';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss'],
})
export class CitiesComponent implements OnInit {
  zipcodeId = '';
  DepartmentCodes = Cities.getdepartmentCodes();
  municipalities: Array<Municipality> = [];
  results: Map<string, Array<Municipality>> = new Map();
  concatRes: any;
  isLoadingSearchResult = false;
  keyword = 'name';
  @ViewChild('autocomplete2') auto: any;
  errorMessage: any;
  breadcrumbs?: Array<BreadcrumbItem>;
  constructor(
    private portalAppointmentService: PortalAppointmentService,
    private spinner: NgxSpinnerService,
    private title: Title,
    private metaTagService: Meta
  ) {}

  ngOnInit(): void {
    this.title.setTitle('Recherche par ville');
    window.scrollTo(0, 0);
    this.metaTagService.updateTag({
      name: 'description',
      content:
        "Trouvez rapidement un rendez-vous pour le renouvellement des passeports ou des cartes d'identité à une des ville interconnectées avec la plateforme nationale ANTS",
    });
    this.isLoadingSearchResult = true;
    this.spinner.show();
    this.portalAppointmentService.getMunicipalities().subscribe(
      (res) => {
        this.municipalities = res;
        if (this.municipalities) {
          let orderedMunicipalities = this.municipalities.sort(
            (a: Municipality, b: Municipality) => Number(a.zip_code) - Number(b.zip_code)
          );
          let i = 0;
          for (i = 0; i < orderedMunicipalities.length && Number((orderedMunicipalities[i].zip_code + '').slice(0, 2)) <= 95; i++) {
            orderedMunicipalities[i] = Cities.cannonizeDptmtAndCity(orderedMunicipalities[i]);
            if (orderedMunicipalities[i].zip_code && !this.results.get((orderedMunicipalities[i].zip_code + '').slice(0, 2))) {
              let zipe_code = (orderedMunicipalities[i].zip_code + '').trim().slice(0, 2);
              let main_municipality: Municipality = {
                city_logo: '',
                city_name: this.DepartmentCodes.get(zipe_code),
                department_name: this.normalize(this.DepartmentCodes.get(zipe_code)?.toLowerCase()),
                distance_km: 0,
                id: '',
                latitude: 0,
                longitude: 0,
                name: '',
                public_entry_address: '',
                zip_code: zipe_code,
                canonized_department_name: Utils.cannonize(this.DepartmentCodes.get(zipe_code)),
              };
              this.results.set(
                (orderedMunicipalities[i].zip_code + '').trim().slice(0, 2),
                [main_municipality].concat(new Array(orderedMunicipalities[i]))
              );
            } else {
              const found = this.results
                .get((orderedMunicipalities[i].zip_code + '').slice(0, 2))
                ?.some((el) => el.canonized_name === orderedMunicipalities[i].canonized_name);
              if (!found) this.results.get((orderedMunicipalities[i].zip_code + '').slice(0, 2))?.push(orderedMunicipalities[i]);
            }
          }

          i = i - 1;
          this.results.set('Outre-Mer', new Array(Cities.cannonizeDptmtAndCity(orderedMunicipalities[i])));
          i += 1;

          while (i < orderedMunicipalities.length) {
            orderedMunicipalities[i] = Cities.cannonizeDptmtAndCity(orderedMunicipalities[i]);

            const found = this.results.get('Outre-Mer')?.some((el) => el.canonized_name === orderedMunicipalities[i].canonized_name);
            if (!found) this.results.get('Outre-Mer')?.push(orderedMunicipalities[i]);

            i += 1;
          }
          this.isLoadingSearchResult = false;
        }
        this.results.delete('Mo');
      },
      (error) => {
        this.errorMessage = {
          title: 'Connexion',
          description: 'Connexion au serveur impossible.',
        };
        this.spinner.hide();
      }
    );

    setTimeout(() => {
      document.getElementsByClassName('input-container')[0]?.children[0].setAttribute('id', 'Recherchez-une-ville');
      let p = document.createElement('label');
      p.textContent = 'Saisissez les premières lettres de votre recherche (département, ville, code postal)';
      p.setAttribute('for', 'Recherchez-une-ville');
      p.style.display = 'none';
      document.getElementsByClassName('input-container')[0]?.prepend(p);
    }, 100);
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Villes',
        url: '/villes',
      },
    ];
  }

  getKeys() {
    if (!this.zipcodeId) return Array.from(this.results.keys());
    return Array.from(this.results.keys()).filter((k) => k === this.zipcodeId);
  }

  normalize(name: string | undefined) {
    if (name) return name.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    return '';
  }

  selectEvent(item: any) {}

  onChangeSearch(val: any) {
    const value: number = Number(val);
    if (!Number.isNaN(value)) {
      this.keyword = 'zip_code';
    } else {
      this.keyword = 'name';
    }
  }

  onClick(id: any) {
    if (id.startsWith('<b>')) id = id.substring(3, 5);

    let element = document.getElementById('accordion' + id);
    if (element !== null) {
      element.scrollIntoView();
      element.setAttribute('aria-expanded', 'true');
    }
  }

  customFilter = (data: any[], query: string): any[] => {
    if (query.length >= 2) {
      if (this.keyword == 'zip_code') {
        const res = Array.from(this.results.values())
          .flat()
          .filter((x) => x.zip_code!.startsWith(query));
        return this.getunique(res);
      } else {
        query = Utils.cannonize(query);
        const res = Array.from(this.results.values())
          .flat()
          .filter((x) => x.department_name?.includes(query) || x.canonized_name?.includes(query));
        return this.getunique(res);
      }
    }
    return [];
  };

  getLength(length: any) {
    return length - 1;
  }

  getunique(res: Array<Municipality>) {
    return res.filter((value, index, self) => {
      return self.findIndex((v) => v.decoded_city_name === value.decoded_city_name) === index;
    });
  }
  toNumber(str: string) {
    return Number(str);
  }
}
