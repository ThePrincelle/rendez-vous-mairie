export interface Editor {
  slug: string;
  name: string;
  api_url: string;
  status: Boolean;
  _test_mode: Boolean;
}
