import json

from fastapi.testclient import TestClient

from tests.mocks.search_criteria import search_criteria


def test_slots_from_position_streaming_ok(client: TestClient):
    with client.websocket_connect("/api/SlotsFromPositionStreaming") as websocket:
        websocket.send_text(
            json.dumps(
                search_criteria,
                default=str,
            )
        )
        """
        data = websocket.receive_text()
        while "step" not in data:
            data = websocket.receive_text()
        json_data = json.loads(data)
        assert json_data["step"] == "end_of_search"
        """
