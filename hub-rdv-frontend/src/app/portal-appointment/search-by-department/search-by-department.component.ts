import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Department } from '@shared/classes/Department';
import { Municipality } from '@shared/classes/municipality';
import { SearchCriteria } from '@shared/classes/search-criteria';
import { DeviceStateService } from '@shared/services/device-state.service';
import { SEOService } from '@shared/services/seoservice.service';
import { DateUtils } from '@shared/utils/date';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subscription } from 'rxjs';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-search-by-department',
  templateUrl: './search-by-department.component.html',
  styleUrls: ['./search-by-department.component.scss'],
})
export class SearchByDepartmentComponent implements OnInit, OnChanges {
  departmentCodes: Map<string, string>;
  municipalities?: Array<Municipality>;
  results: Map<string, Array<Municipality>> = new Map();
  XL: boolean = false;
  private resizeSubscription?: Subscription;
  /**
   * indicator when search result is loading ( for spinner)
   * @type {boolean}
   */
  @Input() isLoadingSearchResult: boolean;

  /**
   * search criteria = form data + latitude and longitude
   * @type {SearchCriteria}
   */
  @Input() searchCriteriaIn?: SearchCriteria;
  /**
   * search criteria emitter to to the portal appointment component
   * @type {EventEmitter}
   */
  @Output() searchCriteria: EventEmitter<SearchCriteria>;
  @Output() latLong: EventEmitter<any>;
  searchRdvForm: FormGroup;
  dateMin: string;
  dateMax: string;
  isDesktop: boolean;
  errorMessage: any;
  department: any;
  breadcrumbs?: Array<BreadcrumbItem>;

  constructor(
    private spinner: NgxSpinnerService,
    private deviceState: DeviceStateService,
    private formBuilder: FormBuilder,
    private router: Router,
    private metaTagService: Meta,
    private titleService: Title,
    private route: ActivatedRoute,
    private seoservice: SEOService
  ) {
    this.isDesktop = !this.deviceState.isMobileResolution();
    this.searchCriteria = new EventEmitter<SearchCriteria>();
    this.latLong = new EventEmitter();
    this.searchRdvForm = new FormGroup({});
    this.isLoadingSearchResult = false;
    this.dateMin = DateUtils.getDefaultStartDate();
    this.dateMax = DateUtils.getEndDateMax(this.dateMin);
    this.errorMessage = {};
    let departmentCodesArray: any[] = [];
    Department.getAllDepartments().forEach((dept) => {
      departmentCodesArray.push([dept['code'], dept['name']]);
    });
    this.departmentCodes = new Map<string, string>(departmentCodesArray);
    this.onScreenChange();
  }

  ngOnInit(): void {
    this.resizeSubscription = this.deviceState.onResize$.subscribe((mobile: any) => {
      this.isDesktop = !mobile;
      this.onScreenChange();
    });
    this.searchRdvForm = this.formBuilder.group({
      start_date: [DateUtils.getDefaultStartDate()],
      end_date: [DateUtils.getDefaultEndDate(null)],
      longitude: [null, [Validators.required]],
      latitude: [null, [Validators.required]],
      reason: ['CNI', [Validators.required]],
      documents_number: [1, [Validators.required]],
    });

    let departemntDecodedName = decodeURI(window.location.pathname).replace('/departement/', '');

    this.department = Department.findDepartmentByDecodedName(departemntDecodedName);

    if (!this.department) {
      this.router.navigateByUrl('/404', { skipLocationChange: true });
      return;
    }

    let title = "Rendez-vous passeport ou carte d'identité " + this.department['name'] + ' ' + this.department['code'];
    this.titleService.setTitle(title);

    this.metaTagService.addTag({
      name: 'title',
      content: title,
    });
    this.metaTagService.updateTag({
      name: 'description',
      content:
        "Trouvez rapidement un rendez-vous pour le renouvellement des passeports ou des cartes d'identité - Département " +
        this.department['name'] +
        ' ' +
        this.department['code'],
    });

    const routeFragment: Observable<any> = this.route.fragment;

    routeFragment.subscribe((fragment) => {
      const searchParams = new URLSearchParams(fragment);

      if (fragment) {
        let start_date = searchParams.get('date-de-debut');
        if (!start_date) start_date = DateUtils.getDefaultStartDate();
        let end_date = searchParams.get('date-de-fin');
        if (!end_date) end_date = DateUtils.getDefaultEndDate(null);
        let documents_number = searchParams.get('nombre-de-personnes');
        if (!documents_number) documents_number = '1';
        let reason = searchParams.get('motif');
        if (!reason) reason = 'CNI';

        const criteria: SearchCriteria = {
          start_date: start_date,
          end_date: end_date,
          radius_km: 20,
          longitude: this.department['longitude'],
          latitude: this.department['latitude'],
          address: ' ',
          documents_number: Number(documents_number),
          reason: 'CNI',
        };

        this.searchRdvForm = this.formBuilder.group({
          start_date: [start_date],
          end_date: [end_date],
          radius_km: [100, [Validators.required]],
          longitude: [this.department['longitude'], [Validators.required]],
          latitude: [this.department['latitude'], [Validators.required]],
          address: [''],
          reason: [reason, [Validators.required]],
          documents_number: [Number(documents_number), [Validators.required]],
        });

        setTimeout(() => {
          this.searchCriteria.emit(criteria);
        }, 100);
      } else {
        this.searchRdvForm = this.formBuilder.group({
          start_date: [DateUtils.getDefaultStartDate()],
          end_date: [DateUtils.getDefaultEndDate(null)],
          radius_km: [100, [Validators.required]],
          longitude: [this.department['longitude'], [Validators.required]],
          latitude: [this.department['latitude'], [Validators.required]],
          address: [''],
          reason: ['CNI', [Validators.required]],
          documents_number: [1, [Validators.required]],
        });
        const criteria: SearchCriteria = {
          start_date: DateUtils.getDefaultStartDate(),
          end_date: DateUtils.getDefaultEndDate(null),
          longitude: this.department['longitude'],
          latitude: this.department['latitude'],
          address: '',
          documents_number: 1,
          reason: 'CNI',
        };

        setTimeout(() => {
          this.searchCriteria.emit(criteria);
        }, 100);
      }
    });

    this.seoservice.updateCanonicalUrl('https://rendezvouspasseport.ants.gouv.fr/departement/' + departemntDecodedName);
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Départements',
        url: '/departements',
      },
      {
        label: `${this.department['code']} - ${
          this.department['name'].charAt(0).toUpperCase() + this.department['name'].slice(1).toLowerCase()
        }`,
        url: '/departement/' + this.department['name'].charAt(0).toUpperCase(),
      },
    ];
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onScreenChange();
  }
  /**
   * Listen to the start date select change and set the end date accordingly (+ 3 months)
   * @param {any} $event : the triggered function event
   */
  handler($event: any) {
    this.searchRdvForm.get('end_date')?.setValue(DateUtils.getDefaultEndDate(this.searchRdvForm.get('start_date')?.value));
    this.dateMax = DateUtils.getEndDateMax(this.searchRdvForm.get('start_date')?.value);
  }

  getByValue(map: Map<string, string>, searchValue: string | undefined) {
    for (let [key, value] of map.entries()) {
      if (value === searchValue) return key;
    }
    return;
  }

  /**
   * on search click, emit search criteria to the portal appointment component
   */
  onSearch(): any {
    if (this.searchRdvForm.valid) {
      const criteria: SearchCriteria = { ...this.searchRdvForm.value };
      this.searchCriteria.emit(criteria);
      this.spinner.show();
    }
  }

  setFormData(start_date: string, end_date: string, latitude: number, longitude: number, documents_number: number, reason: string) {
    this.searchRdvForm.get('start_date')?.setValue(start_date);
    this.searchRdvForm.get('end_date')?.setValue(end_date);
    this.searchRdvForm.get('latitude')?.setValue(latitude);
    this.searchRdvForm.get('longitude')?.setValue(longitude);
    this.searchRdvForm.get('documents_number')?.setValue(documents_number);
    this.searchRdvForm.get('reason')?.setValue(reason);
  }

  normalize(name: string) {
    return name.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  onScreenChange() {
    const screen = this.deviceState.getScreen();
    switch (screen) {
      case 'MD':
        this.XL = false;
        break;
      case 'LG':
        this.XL = false;
        break;
      case 'XL':
        this.XL = true;
    }
  }
}
