import { Component, Input, OnInit } from '@angular/core';
import { Appointement } from '@shared/classes/Appointement';
import { ModalService } from '@shared/services/modal.service';

@Component({
  selector: 'rdv-list-predemande',
  templateUrl: './list-predemande.component.html',
  styleUrls: ['./list-predemande.component.scss'],
})
export class ListPredemandeComponent implements OnInit {
  /**
   * A shared (with the parent (predemande)) map-like object that contains the pré-demande appointement(s) per pré-demande ID .
   * @type {Object.<string, Array<any>>}
   */
  @Input() predemande_results: Map<string, Array<Appointement>> = new Map();
  constructor(private modalService: ModalService) {}

  ngOnInit(): void {}
  /**
   * Date formatting
   * @param {any} d : date
   */
  __getLocaleTimeFormat__(d: any) {
    let date = new Date(d);
    let minutes = ('0' + date.getMinutes()).slice(-2);
    let appointement = date.getHours() + 'h' + minutes;
    return appointement;
  }
  /**
   *   Get the keys of the predemande results
   * @param {Map<string, Array<any>>} map : predemande results
   */
  getKeys(map: Map<string, Array<any>>) {
    return Array.from(map.keys());
  }
  /**
   *   Set the modal (popup) button url link on click
   * @param {string|undefined} url : url of the clicked Accordion (book or cancel)
   */
  setModalUrl(url: any) {
    let link = document.getElementById('slotLink') as HTMLAnchorElement;
    if (link) link.href = url;
  }

  ani($event: any, index: any) {
    let belm = document.getElementById('button-' + index);
    let elm = document.getElementById('accordion-' + index);
    if (elm && belm) elm.classList.toggle('toggle');
  }
}
