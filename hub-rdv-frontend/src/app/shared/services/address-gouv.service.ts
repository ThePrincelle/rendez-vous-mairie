import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AddressGouvService {
  __api: string = environment.address_gouv_api;
  __api_search = environment.address_gouv_api_search;
  constructor(private http: HttpClient) {}

  getAddressByLonAndLat(lon?: number, lat?: number): Observable<any> {
    let params = new HttpParams();
    params = params.append('lat', lat ? lat : 0);
    params = params.append('lon', lon ? lon : 0);
    return this.http.get(this.__api, { params: params });
  }
  getLonAndLatByAddress(search?: string, type?: string): Observable<any> {
    let params = new HttpParams();
    params = params.append('q', search ? search : '');
    params = params.append('type', type ? type : '');
    params = params.append('autocomplete', '1');
    return this.http.get(this.__api_search, { params: params });
  }
}
