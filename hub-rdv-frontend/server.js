const ecsFormat = require('@elastic/ecs-winston-format')
const express = require('express');
require('dotenv').config()
const onFinished = require( "on-finished" );
const winston = require('winston')


const logger = winston.createLogger({
    level: 'info',
    format: ecsFormat(),
    transports: [
      new winston.transports.Console()
    ]
  })


const app = express();
const port = process.env.PORT || '3000';
const path = require('path');

app.use(
	function applyTiming( request, response, next ) {
        var startedAt = Date.now();

        onFinished(
			response,
			function logRequestDuration( error ) {
				var requestDuration = ( Date.now() - startedAt );
                logger.info('Access', {
                    "method": request.method,
                    "path": request.url,
                    "responseTime": requestDuration,
                    "realip": request.headers['x-real-ip'] || request.ip,
                    "referrer": request.get('Referrer'),
                    "source_side": "Frontend"
                })
            });
        next();
    });

app.use(function(req, res, next) {
    res.setHeader("X-Frame-Options", "DENY");
    res.setHeader("Content-Security-Policy", "script-src 'self' 'unsafe-inline' 'unsafe-eval' https://www.google.com https://gstatic.com https://www.gstatic.com; object-src 'self'");
    res.setHeader("X-Content-Type-Options", "nosniff");
    res.setHeader("X-XSS-Protection", "1; mode=block");
    next();
  });

app.get('/robots.txt', function(req, res) {
    if (process.env.ANGULAR_ENVIRONMENT == 'production'){
        res.sendFile(path.resolve('dist/hub-rdv-frontend/robots.txt'));
    } else {
        res.sendFile(path.resolve('dist/hub-rdv-frontend/robots-dev.txt'));
    }
})

app.use(express.static('dist/hub-rdv-frontend/browser'));

app.get('*', function(req,res) {
    res.sendFile(path.resolve('dist/hub-rdv-frontend/index.html'))
});

app.listen(port, () => {
    logger.info(`HUB RDV Frontend app listening on port ${port}`);
});
