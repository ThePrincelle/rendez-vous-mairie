// We use .versionrc.js instead .versionrc to embed python bump script
// https://github.com/conventional-changelog/standard-version
const stringifyPackage = require('stringify-package')
const detectIndent = require('detect-indent')
const detectNewline = require('detect-newline')

const packageJson = {
  filename: 'hub-rdv-frontend/package.json',
  type: 'json'
}

const packageLockJson = {
  filename: 'hub-rdv-frontend/package-lock.json',
  type: 'json'
}

const versionDate = {
  filename: 'hub-rdv-frontend/version-date.json',
  updater: {
    readVersion: function (contents) {
      return JSON.parse(contents).dateRelease;
    },
    writeVersion: function (contents, version) {
    const json = JSON.parse(contents);
    let indent = detectIndent(contents).indent;
    let newline = detectNewline(contents);
    json.dateRelease = (new Date()).toISOString().split('T')[0];
    return stringifyPackage(json, indent, newline);
    }
  }
}

const versionPy = {
  filename: 'hub-rdv-backend/_version.py',
  updater: {
    readVersion: function (contents) {
      let match = /__version__ = "(?<version>.*)"/
      const found = contents.match(match)
      if (!found) {
        throw Error('No matches found for provided')
      }
      if (!found.groups || !found.groups.version) {
        throw Error('The named capture group version was not found.')
      }
      return found.groups.version
    },
    writeVersion: function (_, version) {
      return `__version__ = "${version}"\n`
    }
  }
}

const versionToml = {
  filename: 'hub-rdv-backend/pyproject.toml',
  updater: {
    readVersion: function (contents) {
      let match = /version = "(?<version>.*)"/
      const found = contents.match(match)
      if (!found) {
        throw Error('No matches found for provided')
      }
      if (!found.groups || !found.groups.version) {
        throw Error('The named capture group version was not found.')
      }
      return found.groups.version
    },
    writeVersion: function (_, version) {
      const versionIndex = _.indexOf('version = ') + 10
      endOfLineIndex = _.substring(versionIndex, _.length).indexOf('\n')
      newString = _.replace(_.substring(versionIndex, versionIndex + endOfLineIndex), '')
      result = [newString.slice(0, versionIndex), `"${version}"`, newString.slice(versionIndex)].join('')
      return result
    }
  }
}

module.exports = {
  tagPrefix: '',
  releaseCommitMessageFormat: 'release: version {{currentTag}}',
  bumpFiles: [
    packageJson,
    packageLockJson,
    versionPy,
    versionToml,
    versionDate
  ],
  packageFiles: [packageJson, versionPy, versionToml, versionDate]
}