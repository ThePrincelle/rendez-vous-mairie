import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { DeviceStateService } from '@shared/services/device-state.service';
import { ModalService } from '@shared/services/modal.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'rdv-modal-redirection',
  templateUrl: './modal-redirection.component.html',
  styleUrls: ['./modal-redirection.component.scss'],
})
export class ModalRedirectionComponent implements OnInit {
  @ViewChild('showModalRedirctionButton') showModalRedirctionButton?: ElementRef;
  subscription$?: Subscription;
  url?: string;
  @Input() desktop: boolean;

  constructor(private loadingService: ModalService, private deviceState: DeviceStateService) {
    this.desktop = this.deviceState.isMobileResolution();
  }

  ngOnInit(): void {
    this.subscription$ = this.loadingService.showModalRedirection.subscribe((state) => {
      if (state.showModalRedirection && this.showModalRedirctionButton) {
        this.url = state.url;
        this.showModalRedirctionButton.nativeElement.click();
      }
    });
  }
}
