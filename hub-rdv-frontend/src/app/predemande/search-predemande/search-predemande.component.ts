import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { DeviceStateService } from '@shared/services/device-state.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-search-predemande',
  templateUrl: './search-predemande.component.html',
  styleUrls: ['./search-predemande.component.scss'],
})
export class SearchPredemandeComponent implements OnInit, OnDestroy {
  public predemandes: string = '';
  predemandeForm: FormGroup;
  ValidateError: boolean = false;
  FormatError: boolean = false;
  desktop: boolean = true;
  breadcrumbs?: Array<BreadcrumbItem>;

  RegexFormat = /^([a-zA-Z0-9]{10}[,;:\-/.\s])*[a-zA-Z0-9]{0,10}$/;
  RegexFormatSearch = /^([a-zA-Z0-9]{10}[,;:\-/.\s])*[a-zA-Z0-9]{10}$/;
  @Output() searchCriteria: EventEmitter<any>;
  @Output() searchCriteriaForm: EventEmitter<any>;
  @Input() endOfSearch: boolean | undefined;

  constructor(private formBuilder: FormBuilder, private deviceState: DeviceStateService, private route: ActivatedRoute) {
    this.predemandeForm = this.formBuilder.group({
      predemandes: [null, [this.RegexSearchValidator()]],
    });
    this.searchCriteria = new EventEmitter<any>();
    this.searchCriteriaForm = new EventEmitter<any>();
    this.desktop = !this.deviceState.isMobileResolution();
  }

  ngOnInit(): void {
    const routeFragment: Observable<any> = this.route.fragment;
    if (routeFragment)
      routeFragment.subscribe((fragment) => {
        const searchParams = new URLSearchParams(fragment);
        if (fragment) {
          let ids = searchParams.get('ids');
          if (ids) this.predemandeForm.get('predemandes')?.setValue(ids);
        }
      });
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Retrouvez vos rendez-vous',
        url: '/NumeroPredemande',
      },
    ];
  }

  onSearch(): any {
    if (this.predemandeForm.valid) this.searchCriteria.emit({ ...this.predemandeForm.value });
  }

  RegexSearchValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.value) {
        if (!control.value.toString().match(this.RegexFormat)) this.FormatError = true;
        else this.FormatError = false;

        if (!control.value.toString().match(this.RegexFormatSearch)) {
          this.ValidateError = true;
          return { valide: true };
        }
      }
      this.ValidateError = false;
      this.FormatError = false;
      return null;
    };
  }

  ngOnDestroy() {
    const criteria = { ...this.predemandeForm.value };
    this.searchCriteriaForm.emit({ ...this.predemandeForm.value });
  }
}
